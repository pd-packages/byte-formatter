using ByteFormatter.Runtime.StateSerialize;
using UnityEngine;

namespace ByteFormatter.Runtime
{
	[ByteExtension]
	public static class ByteReaderUnityExtensions
	{
		public static Vector2 ReadVector2(this ByteReader reader) => new(reader.ReadSingle(), reader.ReadSingle());

		public static void SkipVector2(this ByteReader reader) => reader.Skip(8);

		public static Vector2Int ReadVector2Int(this ByteReader reader) => new(reader.ReadInt32(), reader.ReadInt32());

		public static void SkipVector2Int(this ByteReader reader) => reader.Skip(8);

		public static Vector3 ReadVector3(this ByteReader reader)
			=> new(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

		public static void SkipVector3(this ByteReader reader) => reader.Skip(12);

		public static Vector4 ReadVector4(this ByteReader reader)
			=> new(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

		public static void SkipVector4(this ByteReader reader) => reader.Skip(16);

		public static Color ReadColor(this ByteReader reader)
			=> new(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

		public static void SkipColor(this ByteReader reader) => reader.Skip(16);

		public static Color32 ReadColor32(this ByteReader reader)
			=> new(reader.ReadByte(), reader.ReadByte(), reader.ReadByte(), reader.ReadByte());

		public static void SkipColor32(this ByteReader reader) => reader.Skip(4);

		public static Quaternion ReadQuaternion(this ByteReader reader)
			=> new(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

		public static void SkipQuaternion(this ByteReader reader) => reader.Skip(16);

		public static Rect ReadRect(this ByteReader reader)
			=> new(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());

		public static void SkipRect(this ByteReader reader) => reader.Skip(16);

		public static RectInt ReadRectInt(this ByteReader reader)
			=> new(reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32());

		public static void SkipRectInt(this ByteReader reader) => reader.Skip(16);

		public static Plane ReadPlane(this ByteReader reader) => new(reader.ReadVector3(), reader.ReadSingle());

		public static void SkipPlane(this ByteReader reader) => reader.Skip(16);

		public static Bounds ReadBounds(this ByteReader reader) => new(reader.ReadVector3(), reader.ReadVector3());

		public static void SkipBounds(this ByteReader reader) => reader.Skip(24);

		public static Ray ReadRay(this ByteReader reader) => new(reader.ReadVector3(), reader.ReadVector3());

		public static void SkipRay(this ByteReader reader) => reader.Skip(24);

		public static Matrix4x4 ReadMatrix4x4(this ByteReader reader) =>
			new()
			{
				m00 = reader.ReadSingle(),
				m01 = reader.ReadSingle(),
				m02 = reader.ReadSingle(),
				m03 = reader.ReadSingle(),
				m10 = reader.ReadSingle(),
				m11 = reader.ReadSingle(),
				m12 = reader.ReadSingle(),
				m13 = reader.ReadSingle(),
				m20 = reader.ReadSingle(),
				m21 = reader.ReadSingle(),
				m22 = reader.ReadSingle(),
				m23 = reader.ReadSingle(),
				m30 = reader.ReadSingle(),
				m31 = reader.ReadSingle(),
				m32 = reader.ReadSingle(),
				m33 = reader.ReadSingle()
			};

		public static void SkipMatrix4x4(this ByteReader reader) => reader.Skip(64);
	}
}