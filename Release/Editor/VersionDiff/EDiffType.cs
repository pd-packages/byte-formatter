﻿namespace Package.ByteFormatter.VersionDiff
{
	public enum EDiffType
	{
		None,
		Added,
		Removed,
		Changed,
		Moved
	}
}