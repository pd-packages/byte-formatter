﻿using System.Collections.Generic;
using Package.ByteFormatter.VersionDiff.Contexts;

namespace Package.ByteFormatter.VersionDiff
{
	public class VersionData
	{
		public string Version;
		public List<ADataContext> Contexts;
	}
}