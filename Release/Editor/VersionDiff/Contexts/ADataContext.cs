﻿using System.Collections.Generic;

namespace Package.ByteFormatter.VersionDiff.Contexts
{
	public abstract class ADataContext
	{
		public static readonly ADataContext Empty = new EmptyDataContext();

		public abstract string Name { get; }
		public abstract List<ADataContext> ChildContexts { get; }

		protected bool Equals(ADataContext other) => Name == other.Name;

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((ADataContext)obj);
		}

		public override int GetHashCode() => Name != null ? Name.GetHashCode() : 0;

		private class EmptyDataContext : ADataContext
		{
			public override string Name => string.Empty;
			public override List<ADataContext> ChildContexts { get; } = new List<ADataContext>();
		}
	}
}