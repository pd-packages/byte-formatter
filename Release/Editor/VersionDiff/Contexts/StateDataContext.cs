﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime;
using ByteFormatter.Runtime.MigrateHelper;

namespace Package.ByteFormatter.VersionDiff.Contexts
{
	public class StateDataContext : ADataContext
	{
		public override string Name { get; }
		public override List<ADataContext> ChildContexts { get; }

		public StateDataContext(string name, Type type)
		{
			Name = name;
			ChildContexts = type.GetFields()
				.OrderBy(t => t.MetadataToken)
				.Select(CreateContext)
				.ToList();
		}

		private static ADataContext CreateContext(FieldInfo fieldInfo)
		{
			var fieldType = fieldInfo.FieldType;
			var byteDataTypeAccessAttr = fieldType.GetCustomAttribute<ByteDataTypeAccessAttribute>();
			if (byteDataTypeAccessAttr.Type != typeof(IByteConvertable)
			    && byteDataTypeAccessAttr.Type != typeof(IList<IByteConvertable>))
				return new StateFieldDataContext(fieldInfo);

			var genericArgs = fieldType.GetGenericArguments();
			var byteDataType = genericArgs.First();
			var dataVersionAttr = byteDataType.GetCustomAttribute<DataVersionAttribute>();
			return new StateDataContext(
				byteDataTypeAccessAttr.Type != typeof(IList<IByteConvertable>)
					? dataVersionAttr.TypeName
					: $"IList<{dataVersionAttr.TypeName}>",
				byteDataType
			);
		}
	}
}