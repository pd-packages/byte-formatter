﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime;
using ByteFormatter.Runtime.MigrateHelper;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime.Utils;
using Package.ByteFormatter.Generator.Utils;
using UnityEngine;

namespace Package.ByteFormatter.Generator.MigrateHelper
{
	public class MigrateHelperByteDataGenerator : IByteDataGenerator
	{
		private readonly List<ByteDataAccess> _byteDataAccesses;

		public MigrateHelperByteDataGenerator()
		{
			_byteDataAccesses = AssemblyHelper.GetTypes()
				.Where(t => t.HasAttribute<ByteDataTypeAccessAttribute>())
				.Select(s => new ByteDataAccess(s.GetCustomAttribute<ByteDataTypeAccessAttribute>(), s))
				.ToList();
		}

		public string Create(ByteDataContext[] contexts, ByteDataContext context)
		{
			var attribute = context.Attribute;
			context.ClassName = MigrateHelperGeneratorTemplates.ClassName(attribute.Name);

			var properties = context.GetProperties()
				.Where(f => f.HasAttribute<PropertyKeyAttribute>())
				.OrderBy(f => f.GetCustomAttribute<PropertyKeyAttribute>().Key)
				.Select(property => GetGeneratedProperty(contexts, property))
				.ToArray();

			var namespaces = new List<string>();
			namespaces.AddRange(properties.Select(p => p.Namespace));
			namespaces.Add(typeof(IByteData).Namespace);
			namespaces.Add(typeof(ByteReader).Namespace);
			namespaces.Add(typeof(ByteWriter).Namespace);
			var usingDeclarations = namespaces.Where(f => !string.IsNullOrEmpty(f))
				.Distinct()
				.Select(MigrateHelperGeneratorTemplates.Namespace)
				.MergeString();

			var fieldDeclarations = properties
				.Select(p => MigrateHelperGeneratorTemplates.ByteDataField(p.Name, p.ByteDataName))
				.MergeString();

			var transferBlock = properties
				.Select(p => MigrateHelperGeneratorTemplates.TransferMethod(p.Name))
				.MergeString();


			var skipBlock = properties
				.Select(p => MigrateHelperGeneratorTemplates.SkipMethod(p.Name))
				.MergeString();

			return MigrateHelperGeneratorTemplates.Class(
				usingDeclarations,
				context.ClassName,
				context.Attribute.Name,
				Application.version,
				MigrateHelperGeneratorTemplates.PropertyName(Application.version),
				fieldDeclarations,
				transferBlock,
				skipBlock
			);
		}

		private GeneratedProperty GetGeneratedProperty(ByteDataContext[] contexts, PropertyInfo propertyInfo)
		{
			var type = propertyInfo.PropertyType;
			var property = new GeneratedProperty() { Name = propertyInfo.Name };

			if (TryCreateByteConvertableByteData(contexts, type, property))
				return property;

			if (TryCreateByteConvertableByteDataList(contexts, type, property))
				return property;

			var access = _byteDataAccesses.FirstOrDefault(f => f.Attribute.Type == type);
			if (access == null)
				throw new Exception($"[{nameof(MigrateHelperByteDataGenerator)}] No byte data for type {type}");

			property.Namespace = access.Type.Namespace;
			property.ByteDataName = access.Type.Name;
			return property;
		}

		private bool TryCreateByteConvertableByteData(
			ByteDataContext[] contexts,
			Type type,
			GeneratedProperty property
		)
		{
			if (!type.ImplementsInterface<IByteConvertable>())
				return false;

			var byteDataAccess = _byteDataAccesses.First(f => f.Attribute.Type == typeof(IByteConvertable));
			property.Namespace = byteDataAccess.Type.Namespace;
			var byteDataContext = contexts.First(f => f.Type == type);
			var accessName = byteDataAccess.Type.Name.Remove(byteDataAccess.Type.Name.IndexOf('`'));
			var classIdentifier = byteDataContext.ClassName;
			var version = MigrateHelperGeneratorTemplates.PropertyName(Application.version);
			property.ByteDataName = $"{accessName}<{classIdentifier}.{version}_ByteData>";
			return true;
		}

		private bool TryCreateByteConvertableByteDataList(
			ByteDataContext[] contexts,
			Type type,
			GeneratedProperty property
		)
		{
			if (!type.ImplementsInterface<IList>())
				return false;

			var genericArguments = type.GetGenericArguments();
			if (genericArguments.Length != 1)
				return false;

			var genericArgument = genericArguments[0];
			if (!genericArgument.ImplementsInterface<IByteConvertable>())
				return false;

			var byteDataAccess = _byteDataAccesses.First(f => f.Attribute.Type == typeof(IList<IByteConvertable>));
			property.Namespace = byteDataAccess.Type.Namespace;
			var byteDataContext = contexts.First(f => f.Type == genericArgument);
			var accessName = byteDataAccess.Type.Name.Remove(byteDataAccess.Type.Name.IndexOf('`'));
			var classIdentifier = byteDataContext.ClassName;
			var version = MigrateHelperGeneratorTemplates.PropertyName(Application.version);
			property.ByteDataName = $"{accessName}<{classIdentifier}.{version}_ByteData>";
			return true;
		}

		private class GeneratedProperty
		{
			public string Namespace;
			public string Name;
			public string ByteDataName;
		}

		private class ByteDataAccess
		{
			public readonly ByteDataTypeAccessAttribute Attribute;
			public readonly Type Type;

			public ByteDataAccess(ByteDataTypeAccessAttribute attribute, Type type)
			{
				Attribute = attribute;
				Type = type;
			}
		}
	}
}