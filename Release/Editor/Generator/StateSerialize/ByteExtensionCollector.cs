﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime.Utils;
using Package.ByteFormatter.Generator.Utils;
using UnityEngine;

namespace Package.ByteFormatter.Generator.StateSerialize
{
	public class ByteExtensionCollector
	{
		private static readonly Dictionary<Type, ByteMethodInfo> WriterExtensionMethods;
		private static readonly Dictionary<Type, ByteMethodInfo> ReaderExtensionMethods;

		public static IEnumerable<ByteMethodInfo> WriterExtensionMethodInfos => WriterExtensionMethods.Values;
		public static IEnumerable<ByteMethodInfo> ReaderExtensionMethodInfos => ReaderExtensionMethods.Values;

		static ByteExtensionCollector()
		{
			var types = AssemblyHelper.GetTypes()
				.Where(t => t.HasAttribute<ByteExtensionAttribute>())
				.ToArray();

			var writerMethods = types.Select(GetWriterMethods)
				.Aggregate((enumerable1, enumerable2) => enumerable1.Concat(enumerable2))
				.ToList();
			WriterExtensionMethods = writerMethods.Distinct().ToDictionary(method => method.SerializedType);
			var writerExtensionMethodsDuplicates = writerMethods.GroupBy(method => method.SerializedType)
				.Where(methods => methods.Count() > 1)
				.ToArray();

			foreach (var duplicate in writerExtensionMethodsDuplicates)
			{
				var type = duplicate.Key;
				var namespaces = string.Join("\r\n",
					duplicate.Select(s => $"{s.Namespace}.{s.ClassName} -> {s.MethodInfo.Name}"));
				Debug.LogError(
					$"[{nameof(ByteExtensionCollector)}] Duplicate writer extension for type {type.FullName}: \r\n {namespaces}\r\n");
			}

			var readMethods = types.Select(GetReaderMethods)
				.Aggregate((enumerable1, enumerable2) => enumerable1.Concat(enumerable2))
				.ToList();
			ReaderExtensionMethods = readMethods.Distinct().ToDictionary(method => method.SerializedType);

			var readerExtensionMethodsDuplicates = readMethods.GroupBy(method => method.SerializedType)
				.Where(methods => methods.Count() > 1)
				.ToArray();

			foreach (var duplicate in readerExtensionMethodsDuplicates)
			{
				var type = duplicate.Key;
				var namespaces = string.Join("\r\n",
					duplicate.Select(s => $"{s.Namespace}.{s.ClassName} -> {s.MethodInfo.Name}"));
				Debug.LogError(
					$"[{nameof(ByteExtensionCollector)}] Duplicate read extension for type {type.FullName}: \r\n {namespaces}\r\n");
			}
		}

		private static IEnumerable<ByteMethodInfo> GetWriterMethods(Type type)
			=> type.GetMethods()
				.Where(m => m.IsWriteMethod())
				.Select(s => new ByteMethodInfo(
					s,
					type.Namespace,
					type.Name,
					s.GetWriteSerializedType()
				));

		public static ByteMethodInfo GetWriterMethod(Type type)
			=> WriterExtensionMethods.GetValueOrDefault(type);

		public static ByteMethodInfo GetReaderMethod(Type type)
			=> ReaderExtensionMethods.GetValueOrDefault(type);

		private static IEnumerable<ByteMethodInfo> GetReaderMethods(Type type)
			=> type.GetMethods()
				.Where(m => m.IsReadMethod())
				.Select(s => new ByteMethodInfo(
					s,
					type.Namespace,
					type.Name,
					s.GetReadSerializedType()
				));

		public static bool ValidateWrite(Type type) => GetWriterMethod(type) != null;
		
		public static bool ValidateRead(Type type) => GetReaderMethod(type) != null;

		public class ByteMethodInfo
		{
			public readonly MethodInfo MethodInfo;
			public readonly string Namespace;
			public readonly string ClassName;
			public readonly Type SerializedType;

			public ByteMethodInfo(
				MethodInfo methodInfo,
				string ns,
				string className,
				Type serializedType
			)
			{
				MethodInfo = methodInfo;
				Namespace = ns;
				ClassName = className;
				SerializedType = serializedType;
			}

			public string GetMethod(Type parameterType)
			{
				if (!MethodInfo.IsGenericMethod)
					return MethodInfo.Name;

				if (!parameterType.IsGenericType)
				{
					if (!parameterType.IsArray)
						return $"{MethodInfo.Name}<{parameterType.Name}>";

					return $"{MethodInfo.Name}<{parameterType.GetElementType().GetName()}>";
				}

				return
					$"{MethodInfo.Name}<{string.Join(", ", parameterType.GetGenericArguments().Select(s => s.GetName()))}>";
			}

			protected bool Equals(ByteMethodInfo other) => SerializedType == other.SerializedType;

			public override bool Equals(object obj)
			{
				if (ReferenceEquals(null, obj)) return false;
				if (ReferenceEquals(this, obj)) return true;
				if (obj.GetType() != this.GetType()) return false;
				return Equals((ByteMethodInfo)obj);
			}

			public override int GetHashCode() => (SerializedType != null ? SerializedType.GetHashCode() : 0);
		}
	}
}