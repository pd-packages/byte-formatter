namespace Package.ByteFormatter.Generator
{
	public interface IClassSyntax
	{
		string ClassName { get; set; }
		string CompilationUnitSyntax { get; set; }
	}

	public class ClassSyntax : IClassSyntax
	{
		public string ClassName { get; set; }
		public string CompilationUnitSyntax { get; set; }

		public ClassSyntax(string className, string compilationUnitSyntax)
		{
			CompilationUnitSyntax = compilationUnitSyntax;
			ClassName = className;
		}
	}
}