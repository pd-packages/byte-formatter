﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime.Utils;

namespace Package.ByteFormatter.Generator
{
	public static class HelpExtensions
	{
		public static string MergeString(this IEnumerable<string> enumerable)
			=> enumerable.Aggregate((s, s1) => s + "\r\n" + s1);

		public static PropertyInfo[] GetSerializedProperties(this Type type)
			=> type.GetProperties()
				.Where(p => p.HasAttribute<PropertyKeyAttribute>())
				.ToArray();

		public static string GetFriendlyName(this Type type)
		{
			var friendlyName = type.Name;
			if (!type.IsGenericType)
				return friendlyName;

			var iBacktick = friendlyName.IndexOf('`');
			if (iBacktick > 0) friendlyName = friendlyName.Remove(iBacktick);
			friendlyName += "<";
			var typeParameters = type.GetGenericArguments();
			for (var i = 0; i < typeParameters.Length; ++i)
			{
				var typeParamName = GetFriendlyName(typeParameters[i]);
				friendlyName += i == 0 ? typeParamName : "," + typeParamName;
			}

			friendlyName += ">";
			return friendlyName;
		}
	}
}