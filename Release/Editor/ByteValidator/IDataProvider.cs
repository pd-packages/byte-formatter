﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Package.ByteFormatter.ByteValidator
{
	public interface IDataProvider
	{
		bool CanProvide(Type type);

		int GetValueVariants(Type type);

		int GetValueSize(int variantIndex, Type type);

		object CreateValue(int variantIndex, Type type);

		void SetValue(int variantIndex, object obj, PropertyInfo propertyInfo);

		List<string> AssertValueEquals(int variantIndex, object obj, Type type);
	}
}