﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class Vector2IntDataProvider : ADataProvider<Vector2Int>
	{
		protected override Vector2Int[] DataArray { get; } = {Vector2Int.one};
		protected override int[] DaraSizeArray { get; } = {8};
	}
}