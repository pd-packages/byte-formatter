namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class DoubleDataProvider : ADataProvider<double>
	{
		protected override double[] DataArray { get; } = { double.MaxValue };
		protected override int[] DaraSizeArray { get; } = { 8 };
	}
}