﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class CharDataProvider : ADataProvider<char>
	{
		protected override char[] DataArray { get; } = {'?'};
		protected override int[] DaraSizeArray { get; } = {1};
	}
}