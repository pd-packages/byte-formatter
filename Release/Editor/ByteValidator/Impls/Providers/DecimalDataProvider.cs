﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class DecimalDataProvider : ADataProvider<decimal>
	{
		protected override decimal[] DataArray { get; } = { decimal.MaxValue };
		protected override int[] DaraSizeArray { get; } = { 16 };
	}
}