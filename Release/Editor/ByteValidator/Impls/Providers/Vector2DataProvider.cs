﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class Vector2DataProvider : ADataProvider<Vector2>
	{
		protected override Vector2[] DataArray { get; } = {Vector2.one};
		protected override int[] DaraSizeArray { get; } = {8};
	}
}