﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class ULongDataProvider : ADataProvider<ulong>
	{
		protected override ulong[] DataArray { get; } = { ulong.MaxValue };
		protected override int[] DaraSizeArray { get; } = { 8 };
	}
}