﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public class BoundsDataProvider : ADataProvider<Bounds>
	{
		protected override Bounds[] DataArray { get; } = {new Bounds(Vector3.one, Vector3.one)};
		protected override int[] DaraSizeArray { get; } = {24};
	}
}