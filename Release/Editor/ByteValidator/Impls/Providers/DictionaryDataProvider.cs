using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using ByteFormatter.Runtime.Utils;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public class DictionaryDataProvider : IDataProvider
	{
		public bool CanProvide(Type type) => type.IsGenericTypeDefinition(typeof(Dictionary<,>));

		public int GetValueVariants(Type type) => 2;

		public int GetValueSize(int variantIndex, Type type)
		{
			const int countSize = 4;

			if (variantIndex == 0)
				return countSize;

			var (keyType, valueType) = GetDictionaryEntryTypes(type);
			var keySize = DataProviderService.GetValueSize(0, keyType);
			var valueSize = DataProviderService.GetValueSize(0, valueType);
			return countSize + keySize + valueSize;
		}

		public object CreateValue(int variantIndex, Type type)
		{
			var dictionary = (IDictionary)Activator.CreateInstance(type);
			if (variantIndex == 0)
				return dictionary;

			var (keyType, valueType) = GetDictionaryEntryTypes(type);
			var key = DataProviderService.CreateValue(0, keyType);
			var value = DataProviderService.CreateValue(0, valueType);
			dictionary.Add(key, value);
			return dictionary;
		}

		public void SetValue(int variantIndex, object obj, PropertyInfo propertyInfo)
		{
			var data = CreateValue(variantIndex, propertyInfo.PropertyType);
			propertyInfo.SetValue(obj, data);
		}

		public List<string> AssertValueEquals(int variantIndex, object obj, Type type)
		{
			var assertions = new List<string>();
			var dictionary = (IDictionary)obj;
			if (variantIndex == 0 && dictionary.Count > 0)
			{
				assertions.Add($"{obj.GetType().Name} must be zero size.");
			}
			else
			{
				foreach (DictionaryEntry kvp in dictionary)
				{
					var (keyType, valueType) = GetDictionaryEntryTypes(type);
					var keyAssertions = DataProviderService.AssertValueEquals(0, kvp.Key, keyType);
					var valueAssertions = DataProviderService.AssertValueEquals(0, kvp.Value, valueType);
					if (keyAssertions.Count > 1)
						assertions.Add(
							$"{obj.GetType().Name} has invalid key.");

					if (valueAssertions.Count > 1)
						assertions.Add(
							$"{obj.GetType().Name} has invalid value.");
				}
			}

			return assertions;
		}

		private static (Type keyType, Type valueType) GetDictionaryEntryTypes(Type type)
		{
			var genericArguments = type.GetGenericArguments();
			return (genericArguments[0], genericArguments[1]);
		}
	}
}