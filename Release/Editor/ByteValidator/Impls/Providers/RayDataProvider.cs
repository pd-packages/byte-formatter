﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class RayDataProvider : ADataProvider<Ray>
	{
		protected override Ray[] DataArray { get; } = {new Ray(Vector3.up, Vector3.forward)};
		protected override int[] DaraSizeArray { get; } = {24};
	}
}