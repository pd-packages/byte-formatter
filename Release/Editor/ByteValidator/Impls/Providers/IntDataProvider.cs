﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class IntDataProvider : ADataProvider<int>
	{
		protected override int[] DataArray { get; } = {int.MaxValue};
		protected override int[] DaraSizeArray { get; } = {4};
	}
}