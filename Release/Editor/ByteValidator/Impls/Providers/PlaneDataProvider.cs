﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class PlaneDataProvider : ADataProvider<Plane>
	{
		protected override Plane[] DataArray { get; } = {new Plane(Vector3.up, Vector3.forward)};
		protected override int[] DaraSizeArray { get; } = {24};
	}
}