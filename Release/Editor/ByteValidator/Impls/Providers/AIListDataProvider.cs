using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public abstract class AIListDataProvider : IDataProvider
	{
		public int GetValueVariants(Type type) => 2;

		public abstract bool CanProvide(Type type);

		public int GetValueSize(int variantIndex, Type type)
		{
			var elementType = GetElementType(type);
			var dataVariants = DataProviderService.GetValueVariants(elementType);
			var dataSize = 4;

			if (variantIndex == 0)
				return dataSize;

			for (var i = 0; i < dataVariants; i++)
			{
				dataSize += DataProviderService.GetValueSize(i, elementType);
			}

			return dataSize;
		}

		public object CreateValue(int variantIndex, Type type)
		{
			var elementType = GetElementType(type);
			if (variantIndex == 0)
				return CreateInstance(elementType, 0);

			var dataVariants = DataProviderService.GetValueVariants(elementType);
			var instance = CreateInstance(elementType, dataVariants);
			for (var i = 0; i < dataVariants; i++)
			{
				var item = DataProviderService.CreateValue(i, elementType);
				instance[i] = item;
			}

			return instance;
		}

		protected abstract Type GetElementType(Type type);

		protected abstract IList CreateInstance(Type type, int lenght);

		public void SetValue(int variantIndex, object obj, PropertyInfo propertyInfo)
		{
			var instance = CreateValue(variantIndex, propertyInfo.PropertyType);
			propertyInfo.SetValue(obj, instance);
		}

		public List<string> AssertValueEquals(int variantIndex, object obj, Type type)
		{
			if (obj is not IList list)
				throw new Exception(
					$"[{nameof(Vector3DataProvider)}] Data do not provide List");

			var errors = new List<string>();
			var elementType = GetElementType(type);
			for (var i = 0; i < list.Count; i++)
			{
				var item = list[i];
				errors.AddRange(DataProviderService.AssertValueEquals(i, item, elementType));
			}

			return errors;
		}
	}
}