using System;
using System.Collections.Generic;
using System.Reflection;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public class NullableDataProvider : IDataProvider
	{
		public bool CanProvide(Type type) => Nullable.GetUnderlyingType(type) != null;

		public int GetValueVariants(Type type)
			=> DataProviderService.GetValueVariants(Nullable.GetUnderlyingType(type)) + 1;

		public int GetValueSize(int variantIndex, Type type)
		{
			const int hasValueSize = 1;
			if (variantIndex == 0)
				return hasValueSize;

			return DataProviderService.GetValueSize(variantIndex - 1, Nullable.GetUnderlyingType(type)) + hasValueSize;
		}

		public object CreateValue(int variantIndex, Type type)
		{
			if (variantIndex == 0)
				return Activator.CreateInstance(type);

			var valueType = Nullable.GetUnderlyingType(type);
			return Activator.CreateInstance(type, DataProviderService.CreateValue(0, valueType));
		}

		public void SetValue(int variantIndex, object obj, PropertyInfo propertyInfo)
		{
			var value = CreateValue(variantIndex, propertyInfo.PropertyType);
			propertyInfo.SetValue(obj, value);
		}

		public List<string> AssertValueEquals(int variantIndex, object obj, Type type)
		{
			var assertions = new List<string>();

			if (variantIndex == 0)
			{
				var value = CreateValue(0, type);
				if (obj != value)
					assertions.Add($"{obj.GetType().Name} has invalid value");
			}
			else
			{
				var underlyingType = Nullable.GetUnderlyingType(type);
				var value = type.GetProperty("Value").GetValue(obj);
				if (DataProviderService.AssertValueEquals(0, value, underlyingType).Count > 0)
					assertions.Add($"{obj.GetType().Name} has invalid value");
			}

			return assertions;
		}
	}
}