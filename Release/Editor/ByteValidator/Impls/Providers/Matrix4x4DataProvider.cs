﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class Matrix4x4DataProvider : ADataProvider<Matrix4x4>
	{
		protected override Matrix4x4[] DataArray { get; } = {Matrix4x4.identity};
		protected override int[] DaraSizeArray { get; } = {64};
	}
}