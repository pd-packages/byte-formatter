﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class ShortDataProvider : ADataProvider<short>
	{
		protected override short[] DataArray { get; } = { short.MaxValue };
		protected override int[] DaraSizeArray { get; } = { 2 };
	}
}