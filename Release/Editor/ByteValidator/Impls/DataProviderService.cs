﻿using System;
using System.Collections.Generic;
using System.Linq;
using ByteFormatter.Runtime.Utils;
using Package.ByteFormatter.Generator.Utils;
using UnityEditor;

namespace Package.ByteFormatter.ByteValidator.Impls
{
	[InitializeOnLoad]
	public static class DataProviderService
	{
		private static readonly List<IDataProvider> DataProviders;

		static DataProviderService()
		{
			DataProviders = AssemblyHelper.GetTypes()
				.Where(type => type.ImplementsInterface<IDataProvider>() && !type.IsAbstract)
				.Select(Activator.CreateInstance)
				.Cast<IDataProvider>()
				.ToList();
		}

		public static int GetValueVariants(Type type) 
			=> GetProvider(type).GetValueVariants(type);

		public static int GetValueSize(int variantIndex, Type type) 
			=> GetProvider(type).GetValueSize(variantIndex, type);

		public static object CreateValue(int variantIndex, Type type) 
			=> GetProvider(type).CreateValue(variantIndex, type);

		public static List<string> AssertValueEquals(int variantIndex, object obj, Type type) 
			=> GetProvider(type).AssertValueEquals(variantIndex, obj, type);

		private static IDataProvider GetProvider(Type type)
		{
			var provider = DataProviders.FirstOrDefault(f => f.CanProvide(type));
			if (provider == null)
				throw new Exception(
					$"[{nameof(DataProviderService)}] Cannot find provider for data: {type.FullName}");

			return provider;
		}
	}
}