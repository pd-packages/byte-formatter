# Changelog

---

## [v3.0.0](https://gitlab.com/pd-packages/byte-formatter/-/tags/v2.0.2)

### Added

- Incremental code generator
- Serialization collections in generics (example `List<List<T>>`)

---

## [v2.0.2](https://gitlab.com/pd-packages/byte-formatter/-/tags/v2.0.2)

### Added

- AsSpan method to ByteWriter

---

## [v2.0.1](https://gitlab.com/pd-packages/byte-formatter/-/tags/v2.0.1)

### Fixed

- Validate write and read methods before generate backing AOT methods

---

## [v2.0.0](https://gitlab.com/pd-packages/byte-formatter/-/tags/v2.0.0)

### Changed

- Change byte save structure and make possible to add or remove data without migration

---

## [v1.1.0](https://gitlab.com/pd-packages/byte-formatter/-/tags/v1.1.0)

### Changed

- Move a project to package

---

## [v0.0.0](https://gitlab.com/pd-packages/byte-formatter/-/tags/v0.0.0)

### Added

- For new features.

### Changed

- For changes in existing functionality.

### Deprecated

- For soon-to-be removed features.

### Removed

- For now removed features.

### Fixed

- For any bug fixes.

### Security

- In case of vulnerabilities.
