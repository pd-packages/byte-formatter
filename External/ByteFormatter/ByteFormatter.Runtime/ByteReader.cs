using System;
using System.Collections.Generic;
using System.Text;
using ByteFormatter.Runtime.StateSerialize;

namespace ByteFormatter.Runtime
{
	[ByteExtension]
	public sealed class ByteReader : ICloneable
	{
		private readonly ByteBuffer _buffer;

		public ByteReader()
		{
			_buffer = new ByteBuffer();
		}

		public ByteReader(byte[] buffer)
		{
			_buffer = new ByteBuffer(buffer);
		}

		private ByteReader(ByteBuffer buffer)
		{
			_buffer = buffer;
		}

		public int Position
		{
			get => _buffer.Position;
			set => _buffer.Position = value;
		}

		public int Length => _buffer.Length;

		public void SeekZero() => _buffer.SeekZero();

		public void Skip(int length) => _buffer.Skip(length);

		public void Replace(byte[] buffer) => _buffer.Replace(buffer);

		public byte ReadByte() => _buffer.ReadByte();

		public void SkipByte() => _buffer.Skip(1);

		public sbyte ReadSByte() => (sbyte)_buffer.ReadByte();

		public void SkipSByte() => _buffer.Skip(1);

		public short ReadInt16()
			=> (short)(ushort)((ushort)(0U | _buffer.ReadByte()) |
			                   (uint)(ushort)((uint)_buffer.ReadByte() << 8));

		public void SkipInt16() => _buffer.Skip(2);

		public ushort ReadUInt16()
			=> (ushort)((ushort)(0U | _buffer.ReadByte()) |
			            (uint)(ushort)((uint)_buffer.ReadByte() << 8));

		public void SkipUInt16() => _buffer.Skip(2);

		public int ReadInt32()
			=> (int)(0U | _buffer.ReadByte() | ((uint)_buffer.ReadByte() << 8) |
			         ((uint)_buffer.ReadByte() << 16) | ((uint)_buffer.ReadByte() << 24));

		public void SkipInt32() => _buffer.Skip(4);

		public uint ReadUInt32()
			=> 0U | _buffer.ReadByte() | ((uint)_buffer.ReadByte() << 8) |
			   ((uint)_buffer.ReadByte() << 16) | ((uint)_buffer.ReadByte() << 24);

		public void SkipUInt32() => _buffer.Skip(4);

		public long ReadInt64()
			=> (long)(0UL | _buffer.ReadByte() | ((ulong)_buffer.ReadByte() << 8) |
			          ((ulong)_buffer.ReadByte() << 16) | ((ulong)_buffer.ReadByte() << 24) |
			          ((ulong)_buffer.ReadByte() << 32) | ((ulong)_buffer.ReadByte() << 40) |
			          ((ulong)_buffer.ReadByte() << 48) | ((ulong)_buffer.ReadByte() << 56));

		public void SkipInt64() => _buffer.Skip(8);

		public ulong ReadUInt64()
			=> 0UL | _buffer.ReadByte() | ((ulong)_buffer.ReadByte() << 8) |
			   ((ulong)_buffer.ReadByte() << 16) | ((ulong)_buffer.ReadByte() << 24) |
			   ((ulong)_buffer.ReadByte() << 32) | ((ulong)_buffer.ReadByte() << 40) |
			   ((ulong)_buffer.ReadByte() << 48) | ((ulong)_buffer.ReadByte() << 56);


		public void SkipUInt64() => _buffer.Skip(8);

		public decimal ReadDecimal()
			=> new(new[]
			{
				ReadInt32(),
				ReadInt32(),
				ReadInt32(),
				ReadInt32()
			});

		public void SkipDecimal() => _buffer.Skip(16);

		public float ReadSingle() => FloatConversion.ToSingle(ReadUInt32());

		public void SkipSingle() => _buffer.Skip(4);

		public double ReadDouble() => FloatConversion.ToDouble(ReadUInt64());

		public void SkipDouble() => _buffer.Skip(8);

		public char ReadChar() => (char)_buffer.ReadByte();

		public void SkipChar() => _buffer.Skip(1);

		public bool ReadBoolean() => _buffer.ReadByte() == 1;

		public void SkipBoolean() => _buffer.Skip(1);

		public byte[] ReadBytes(int count)
		{
			if (count < 0)
				throw new IndexOutOfRangeException("NetworkReader ReadBytes " + count);
			var buffer = new byte[count];
			_buffer.ReadBytes(buffer, count);
			return buffer;
		}

		public void SkipBytesAndSize()
		{
			var length = ReadInt32();
			_buffer.Skip(length);
		}

		public string ReadString()
		{
			var size = ReadInt32();
			return size != 0 ? Encoding.UTF8.GetString(_buffer.ReadSpawn(size)) : string.Empty;
		}

		public void SkipString() => SkipBytesAndSize();

		public Guid ReadGuid() => new(_buffer.ReadSpawn(16));

		public void SkipGuid() => Skip(16);

		public T? ReadNullable<T>()
			where T : struct
		{
			if (!ReadBoolean())
				return null;

			return ByteTypeCache<T>.Read(this);
		}

		public List<T> ReadList<T>()
		{
			var count = ReadInt32();
			var list = new List<T>(count);
			for (var i = 0; i < count; i++)
			{
				var value = ByteTypeCache<T>.Read(this);
				list.Add(value);
			}

			return list;
		}

		public Dictionary<TKey, TValue> ReadDictionary<TKey, TValue>()
		{
			var count = ReadInt32();
			var dictionary = new Dictionary<TKey, TValue>();
			for (var i = 0; i < count; i++)
			{
				var key = ByteTypeCache<TKey>.Read(this);
				var value = ByteTypeCache<TValue>.Read(this);
				dictionary.Add(key, value);
			}

			return dictionary;
		}

		public T[] ReadArray<T>()
		{
			var count = ReadInt32();
			var array = new T[count];
			for (var i = 0; i < count; i++)
			{
				var value = ByteTypeCache<T>.Read(this);
				array[i] = value;
			}

			return array;
		}

		public override string ToString() => _buffer.ToString();

		public T ReadByteConvertable<T>() where T : IByteConvertable, new()
		{
			var convertable = new T();
			convertable.FromByte(this);
			return convertable;
		}

		object ICloneable.Clone() => Clone();

		public ByteReader Clone() => new(_buffer.Clone());
	}
}