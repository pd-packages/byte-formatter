﻿namespace ByteFormatter.Runtime
{
	public interface IByteConvertable
	{
		void ToByte(ByteWriter writer);

		void FromByte(ByteReader reader);
	}
}