﻿using System;

namespace ByteFormatter.Runtime.StateSerialize
{
	[AttributeUsage(AttributeTargets.Property)]
	public class PropertyKeyAttribute : Attribute
	{
		public readonly ushort Key;

		public PropertyKeyAttribute(ushort key)
		{
			Key = key;
		}
	}
}