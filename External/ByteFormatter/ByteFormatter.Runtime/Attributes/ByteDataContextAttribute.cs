﻿using System;

namespace ByteFormatter.Runtime.StateSerialize
{
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class ByteDataContextAttribute : Attribute
	{
		public readonly string Name;

		public ByteDataContextAttribute(string name)
		{
			Name = name;
		}
	}
}