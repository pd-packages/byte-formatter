namespace ByteFormatter.Runtime
{
	public struct ObjectMap
	{
		public const int PROPERTIES_COUNT_SIZE = 2;
		public const int PROPERTY_INFO_KEY_SIZE = 2;
		public const int PROPERTY_INFO_DATA_SIZE = 4;
		public const int PROPERTY_INFO_SIZE = PROPERTY_INFO_KEY_SIZE + PROPERTY_INFO_DATA_SIZE;
	}
}