﻿namespace ByteFormatter.Runtime.MigrateHelper
{
	public interface IByteData
	{
		void Transfer(ByteReader reader, ByteWriter writer);

		void Skip(ByteReader reader);
	}
}