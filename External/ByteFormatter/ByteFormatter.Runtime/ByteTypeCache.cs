using System;

namespace ByteFormatter.Runtime
{
	public class ByteTypeCache<T>
	{
		public static Action<ByteWriter, T> Write;
		public static Func<ByteReader, T> Read;

		public static void Init(Action<ByteWriter, T> write, Func<ByteReader, T> read)
		{
			Write = write;
			Read = read;
		}
	}
}