using System.Runtime.CompilerServices;

namespace ByteFormatter.Runtime
{
	public readonly struct ObjectMapWriter
	{
		public readonly ByteWriter Writer;
		public readonly int ObjectBeginPosition;

		public ObjectMapWriter(ByteWriter writer, ushort propertyCount)
		{
			Writer = writer;
			Writer.Write(propertyCount);
			ObjectBeginPosition = Writer.Position;
		}
	}

	public static class ObjectMapWriterExtensions
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WritePropertyKey(this ref ObjectMapWriter map, ushort propertyKey)
		{
			map.Writer.Write(propertyKey);
			map.Writer.Write(0);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void WritePropertyOffset(this ref ObjectMapWriter map, int positionIndex)
		{
			var currentPosition = map.Writer.Position;
			map.Writer.Position = map.ObjectBeginPosition
			                      + positionIndex * ObjectMap.PROPERTY_INFO_SIZE
			                      + ObjectMap.PROPERTY_INFO_KEY_SIZE;
			map.Writer.Write(currentPosition - map.ObjectBeginPosition);
			map.Writer.Position = currentPosition;
		}
	}
}