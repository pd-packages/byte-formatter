using System;

namespace ByteFormatter.Runtime
{
	public sealed class ByteBuffer : ICloneable
	{
		private const int INITIAL_SIZE = 64;

		private byte[] _buffer;

		public ByteBuffer() : this(new byte[INITIAL_SIZE])
		{
		}

		public ByteBuffer(byte[] buffer, int position = 0)
		{
			_buffer = buffer;
			Position = position;
		}

		public int Position;

		public int Length => _buffer.Length;

		public byte ReadByte()
		{
			if (Position >= _buffer.Length)
				throw new IndexOutOfRangeException("NetworkReader:ReadByte out of range:" + ToString());
			return _buffer[Position++];
		}

		public void ReadBytes(byte[] buffer, int count)
		{
			if (Position + count > _buffer.Length)
				throw new IndexOutOfRangeException("NetworkReader:ReadBytes out of range: (" + count + ") " +
				                                   ToString());
			for (var index = 0; (uint)index < count; ++index)
				buffer[index] = _buffer[Position + index];
			Position += count;
		}

		public ReadOnlySpan<byte> ReadSpawn(int count)
		{
			var span = new ReadOnlySpan<byte>(_buffer, Position, count);
			Position += count;
			return span;
		}

		public byte[] AsArraySegment()
		{
			var bytes = new byte[Position];
			Array.Copy(_buffer, bytes, Position);
			return bytes;
		}

		public Span<byte> AsSpan() => new(_buffer, 0, Position);

		public void WriteByte(byte value)
		{
			WriteCheckForSpace(1);
			_buffer[Position] = value;
			++Position;
		}

		public void WriteByte2(byte value0, byte value1)
		{
			WriteCheckForSpace(2);
			_buffer[Position] = value0;
			_buffer[Position + 1U] = value1;
			Position += 2;
		}

		public void WriteByte4(byte value0, byte value1, byte value2, byte value3)
		{
			WriteCheckForSpace(4);
			_buffer[Position] = value0;
			_buffer[Position + 1] = value1;
			_buffer[Position + 2] = value2;
			_buffer[Position + 3] = value3;
			Position += 4;
		}

		public void WriteByte8(
			byte value0,
			byte value1,
			byte value2,
			byte value3,
			byte value4,
			byte value5,
			byte value6,
			byte value7
		)
		{
			WriteCheckForSpace(8);
			_buffer[Position] = value0;
			_buffer[Position + 1] = value1;
			_buffer[Position + 2] = value2;
			_buffer[Position + 3] = value3;
			_buffer[Position + 4] = value4;
			_buffer[Position + 5] = value5;
			_buffer[Position + 6] = value6;
			_buffer[Position + 7] = value7;
			Position += 8;
		}

		public void WriteBytesAtOffset(byte[] buffer, int targetOffset, int count)
		{
			var num = count + targetOffset;
			WriteCheckForSpace(num);
			if (targetOffset == 0 && count == buffer.Length)
				buffer.CopyTo(_buffer, Position);
			else
				for (var index = 0; index < count; ++index)
					_buffer[targetOffset + index] = buffer[index];

			if (num <= Position)
				return;
			Position = num;
		}

		public void WriteBytes(byte[] buffer, int count)
		{
			WriteCheckForSpace(count);
			if (count == buffer.Length)
				buffer.CopyTo(_buffer, Position);
			else
				for (var index = 0; index < count; ++index)
					_buffer[Position + index] = buffer[index];

			Position += count;
		}

		private void WriteCheckForSpace(int count)
		{
			var requiredSize = Position + count;
			if (requiredSize < _buffer.Length)
				return;

			var length = _buffer.Length;
			do
			{
				length <<= 1;
			} while (length < requiredSize);

			Array.Resize(ref _buffer, length);
		}

		public void SeekZero() => Position = 0;

		public void Skip(int length) => Position += length;

		public void Replace(byte[] buffer)
		{
			_buffer = buffer;
			Position = 0;
		}

		public override string ToString() => $"ByteBuffer size:{_buffer.Length} position:{Position}";

		object ICloneable.Clone() => Clone();

		public ByteBuffer Clone() => new(_buffer, Position);

		public ByteBuffer Copy()
		{
			var bytes = new byte[_buffer.Length];
			_buffer.CopyTo(bytes, 0);
			return new ByteBuffer(bytes, Position);
		}
	}
}