using System.Runtime.CompilerServices;

namespace ByteFormatter.Runtime
{
	public readonly struct ObjectMapReader
	{
		public readonly ByteReader Reader;
		public readonly int PropertyCount;
		public readonly int ObjectBeginPosition;

		public ObjectMapReader(ByteReader reader)
		{
			Reader = reader;
			PropertyCount = reader.ReadUInt16();
			ObjectBeginPosition = reader.Position;
		}
	}

	public static class ObjectMapReaderExtensions
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TryReadProperty(this ref ObjectMapReader map, ushort propertyKey)
		{
			map.Reader.Position = map.ObjectBeginPosition;
			for (var i = 0; i < map.PropertyCount; i++)
			{
				var readKey = map.Reader.ReadUInt16();
				if (readKey != propertyKey)
				{
					map.Reader.SkipInt32();
					continue;
				}

				if (i == 0)
				{
					map.Reader.Position = map.ObjectBeginPosition
					                      + map.PropertyCount
					                      * ObjectMap.PROPERTY_INFO_SIZE;
					return true;
				}

				map.Reader.Position -= ObjectMap.PROPERTY_INFO_SIZE;
				map.Reader.Position = map.ObjectBeginPosition + map.Reader.ReadInt32();
				return true;
			}

			return false;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void EndReadObject(this ref ObjectMapReader map)
		{
			if (map.PropertyCount == 0)
			{
				map.Reader.Position = map.ObjectBeginPosition;
				return;
			}

			map.Reader.Position = map.ObjectBeginPosition
			                      + map.PropertyCount * ObjectMap.PROPERTY_INFO_SIZE
			                      - ObjectMap.PROPERTY_INFO_DATA_SIZE;
			map.Reader.Position = map.ObjectBeginPosition + map.Reader.ReadInt32();
		}
	}
}