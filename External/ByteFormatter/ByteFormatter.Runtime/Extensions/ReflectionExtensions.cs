﻿using System;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime.StateSerialize;

namespace ByteFormatter.Runtime.Utils
{
	public static class ReflectionExtensions
	{
		public static bool ImplementsInterface<T>(this Type type)
			=> !type.IsInterface && type.GetInterface(typeof(T).FullName) != null;

		public static bool HasAttribute<T>(this MemberInfo type)
			where T : Attribute
			=> type.GetCustomAttribute<T>() != null;

		public static bool IsWriteMethod(this MethodInfo methodInfo)
		{
			if (!methodInfo.Name.Equals("Write") || methodInfo.ReturnType != typeof(void))
				return false;

			var parameterInfos = methodInfo.GetParameters();
			if (methodInfo.IsStatic)
				return parameterInfos.Length == 2 && parameterInfos[0].ParameterType == typeof(ByteWriter);

			return parameterInfos.Length == 1;
		}

		public static bool IsReadMethod(this MethodInfo methodInfo)
		{
			if (!methodInfo.Name.StartsWith("Read", StringComparison.Ordinal))
				return false;

			var parameterInfos = methodInfo.GetParameters();
			if (methodInfo.IsStatic)
				return parameterInfos.Length == 1 && parameterInfos[0].ParameterType == typeof(ByteReader);

			return parameterInfos.Length == 0;
		}

		public static Type GetWriteSerializedType(this MethodInfo methodInfo)
		{
			var type = methodInfo.IsStatic
				? methodInfo.GetParameters()[1].ParameterType
				: methodInfo.GetParameters()[0].ParameterType;

			return methodInfo.IsGenericMethod ? type.GetSerializedType() : type;
		}

		public static Type GetReadSerializedType(this MethodInfo methodInfo)
		{
			if (!methodInfo.IsGenericMethod)
				return methodInfo.ReturnType;

			if (methodInfo.Name.StartsWith("ReadByteConvertable"))
				return typeof(IByteConvertable);

			return methodInfo.ReturnType.GetSerializedType();
		}

		public static Type GetSerializedType(this Type type)
		{
			if (type.IsGenericType)
				return type.GetGenericTypeDefinition();

			if (type.IsArray)
				return typeof(Array);

			return type.IsByteConvertable() ? typeof(IByteConvertable) : type;
		}

		public static bool IsGenericTypeDefinition(this Type type, Type genericType)
			=> type.IsGenericType && type.GetGenericTypeDefinition() == genericType;

		public static bool IsByteConvertable(this Type type)
			=> type.HasAttribute<ByteDataContextAttribute>() || type.ImplementsInterface<IByteConvertable>();

		public static string GetName(this Type type)
		{
			if (!type.IsGenericType)
				return type.Name;

			if (type.IsNullable())
				return $"{Nullable.GetUnderlyingType(type).GetName()}?";

			var name = type.Name.Split('`', StringSplitOptions.RemoveEmptyEntries)[0];
			return $"{name}<{string.Join(", ", type.GetGenericArguments().Select(GetName))}>";
		}

		public static bool IsNullable(this Type type) => Nullable.GetUnderlyingType(type) != null;
	}
}