using System;
using System.Collections.Generic;
using System.Text;
using ByteFormatter.Runtime.StateSerialize;

namespace ByteFormatter.Runtime
{
	[ByteExtension]
	public sealed class ByteWriter
	{
		private static UIntFloat _floatConverter;

		private readonly ByteBuffer _buffer;

		public ByteWriter()
		{
			_buffer = new ByteBuffer();
		}

		public ByteWriter(byte[] buffer)
		{
			_buffer = new ByteBuffer(buffer);
		}

		private ByteWriter(ByteBuffer buffer)
		{
			_buffer = buffer;
		}

		public int Position
		{
			get => _buffer.Position;
			set => _buffer.Position = value;
		}

		public byte[] ToArray() => _buffer.AsArraySegment();

		public Span<byte> AsSpan() => _buffer.AsSpan();

		public void Write(char value) => _buffer.WriteByte((byte)value);

		public void Write(byte value) => _buffer.WriteByte(value);

		public void Write(sbyte value) => _buffer.WriteByte((byte)value);

		public void Write(short value)
			=> _buffer.WriteByte2(
				(byte)((uint)value & byte.MaxValue),
				(byte)((value >> 8) & byte.MaxValue)
			);

		public void Write(ushort value)
			=> _buffer.WriteByte2(
				(byte)(value & (uint)byte.MaxValue),
				(byte)((value >> 8) & byte.MaxValue)
			);

		public void Write(int value)
			=> _buffer.WriteByte4(
				(byte)(value & byte.MaxValue),
				(byte)((value >> 8) & byte.MaxValue),
				(byte)((value >> 16) & byte.MaxValue),
				(byte)((value >> 24) & byte.MaxValue)
			);

		public void Write(uint value)
			=> _buffer.WriteByte4(
				(byte)(value & byte.MaxValue),
				(byte)((value >> 8) & byte.MaxValue),
				(byte)((value >> 16) & byte.MaxValue),
				(byte)((value >> 24) & byte.MaxValue)
			);

		public void Write(long value)
			=> _buffer.WriteByte8(
				(byte)((ulong)value & byte.MaxValue),
				(byte)((ulong)(value >> 8) & byte.MaxValue),
				(byte)((ulong)(value >> 16) & byte.MaxValue),
				(byte)((ulong)(value >> 24) & byte.MaxValue),
				(byte)((ulong)(value >> 32) & byte.MaxValue),
				(byte)((ulong)(value >> 40) & byte.MaxValue),
				(byte)((ulong)(value >> 48) & byte.MaxValue),
				(byte)((ulong)(value >> 56) & byte.MaxValue)
			);

		public void Write(ulong value)
			=> _buffer.WriteByte8(
				(byte)(value & byte.MaxValue),
				(byte)((value >> 8) & byte.MaxValue),
				(byte)((value >> 16) & byte.MaxValue),
				(byte)((value >> 24) & byte.MaxValue),
				(byte)((value >> 32) & byte.MaxValue),
				(byte)((value >> 40) & byte.MaxValue),
				(byte)((value >> 48) & byte.MaxValue),
				(byte)((value >> 56) & byte.MaxValue)
			);

		public void Write(float value)
		{
			_floatConverter.floatValue = value;
			Write(_floatConverter.intValue);
		}

		public void Write(double value)
		{
			_floatConverter.doubleValue = value;
			Write(_floatConverter.longValue);
		}

		public void Write(decimal value)
		{
			var bits = decimal.GetBits(value);
			Write(bits[0]);
			Write(bits[1]);
			Write(bits[2]);
			Write(bits[3]);
		}

		public void Write(bool value) => _buffer.WriteByte(value ? (byte)1 : (byte)0);

		public void InsertBytes(byte[] bytes)
			=> _buffer.WriteBytes(bytes, bytes.Length);

		public void InsertBytes(byte[] bytes, int count)
			=> _buffer.WriteBytes(bytes, count);

		public void InsertBytes(byte[] bytes, int offset, int count)
			=> _buffer.WriteBytesAtOffset(bytes, offset, count);

		public unsafe void Write(string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				Write(0);
				return;
			}

			var size = Encoding.UTF8.GetByteCount(value);
			Span<byte> span = stackalloc byte[size];
			Encoding.UTF8.GetBytes(value, span);
			Write(size);
			for (var i = 0; i < span.Length; i++)
				Write(span[i]);
		}

		public unsafe void Write(Guid guid)
		{
			Span<byte> span = stackalloc byte[16];
			guid.TryWriteBytes(span);
			for (var i = 0; i < span.Length; i++)
				Write(span[i]);
		}

		public void Write<T>(T? value)
			where T : struct
		{
			if (!value.HasValue)
			{
				Write(false);
				return;
			}

			Write(true);
			ByteTypeCache<T>.Write(this, value.Value);
		}

		public void Write<T>(T[]? array)
		{
			if (array == null || array.Length == 0)
			{
				Write(0);
				return;
			}

			Write(array.Length);
			for (var i = 0; i < array.Length; i++)
				ByteTypeCache<T>.Write(this, array[i]);
		}

		public void Write<T>(List<T>? list)
		{
			if (list == null || list.Count == 0)
			{
				Write(0);
				return;
			}

			Write(list.Count);
			for (var i = 0; i < list.Count; i++)
				ByteTypeCache<T>.Write(this, list[i]);
		}

		public void Write<TKey, TValue>(Dictionary<TKey, TValue>? dictionary)
		{
			if (dictionary == null || dictionary.Count == 0)
			{
				Write(0);
				return;
			}

			Write(dictionary.Count);
			foreach (var kvp in dictionary)
			{
				ByteTypeCache<TKey>.Write(this, kvp.Key);
				ByteTypeCache<TValue>.Write(this, kvp.Value);
			}
		}

		public void Write<T>(T convertable) where T : IByteConvertable
			=> convertable.ToByte(this);

		public void SeekZero() => _buffer.SeekZero();

		public void Skip(int length) => _buffer.Skip(length);

		public ByteWriter Copy() => new(_buffer.Copy());
	}
}