using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;

namespace ByteFormatter.Generator.Extensions
{
	public static class SymbolExtensions
	{
		public static IEnumerable<INamedTypeSymbol> GetNamespaceTypes(this INamespaceSymbol namespaceSymbol)
		{
			foreach (var member in namespaceSymbol.GetMembers())
			{
				if (member is INamedTypeSymbol namedType)
				{
					yield return namedType;
				}

				if (member is INamespaceSymbol nestedNamespace)
				{
					foreach (var nestedType in nestedNamespace.GetNamespaceTypes())
					{
						yield return nestedType;
					}
				}
			}
		}

		public static bool IsByteExtension(this INamedTypeSymbol namedTypeSymbol)
		{
			var attributes = namedTypeSymbol.GetAttributes();
			foreach (var attribute in attributes)
			{
				if (attribute.AttributeClass?.Name == TypesLookup.ByteExtensionAttribute)
					return true;
			}

			return false;
		}

		public static bool IsSerializedType(this INamedTypeSymbol namedTypeSymbol)
			=> namedTypeSymbol.GetAttributes()
				.Any(attr => attr.AttributeClass?.Name == TypesLookup.ByteDataContextAttribute);

		public static bool IsNullable(this INamedTypeSymbol typeSymbol)
			=> typeSymbol is { Name: "Nullable" };

		public static string GetNamespace(this ITypeSymbol typeSymbol)
			=> typeSymbol.ContainingNamespace == null || string.IsNullOrEmpty(typeSymbol.ContainingNamespace.Name)
				? string.Empty
				: typeSymbol.ContainingNamespace.ToDisplayString();

		public static string GetNameWithNamespace(this ITypeSymbol typeSymbol)
		{
			var ns = typeSymbol.GetNamespace();
			return string.IsNullOrEmpty(ns) ? typeSymbol.Name : ns + "." + typeSymbol.Name;
		}

		public static string GetFullName(this ITypeSymbol typeSymbol)
		{
			if (typeSymbol is IArrayTypeSymbol arrayTypeSymbol)
				return arrayTypeSymbol.ElementType.GetFullName() + "[]";

			var name = typeSymbol.GetNameWithNamespace();
			if (typeSymbol is not INamedTypeSymbol namedTypeSymbol)
				return name;

			return namedTypeSymbol.IsGenericType
				? $"{name}<{string.Join(", ", namedTypeSymbol.TypeArguments.Select(s => s.GetFullName()))}>"
				: name;
		}

		public static string GetSerializableType(this ITypeSymbol typeSymbol)
		{
			if (typeSymbol is IArrayTypeSymbol)
				return "System.Array";

			if (typeSymbol is ITypeParameterSymbol parameterSymbol)
			{
				var constraintTypes = parameterSymbol.ConstraintTypes;
				if (constraintTypes.Length == 1 && constraintTypes[0].Name == TypesLookup.IByteConvertable)
					return TypesLookup.IByteConvertableFullName;
			}

			if (typeSymbol is INamedTypeSymbol namedTypeSymbol && namedTypeSymbol.IsSerializedType())
				return TypesLookup.IByteConvertableFullName;

			return typeSymbol.GetNameWithNamespace();
		}
	}
}