using Microsoft.CodeAnalysis;

namespace ByteFormatter.Generator.States
{
	public class StatePropertyData
	{
		public ushort Key;
		public string Name;
		public ITypeSymbol Type;
	}
}