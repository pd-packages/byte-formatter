namespace ByteFormatter.Generator.States
{
	public class StateGenerationData
	{
		public string Namespace;
		public string Name;
		public StatePropertyData[] Properties;

		public string GetFileName() => !string.IsNullOrEmpty(Namespace)
			? Namespace + "." + Name
			: Name;
	}
}