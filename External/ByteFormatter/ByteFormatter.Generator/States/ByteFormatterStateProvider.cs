using System;
using System.Collections.Generic;
using System.Linq;
using ByteFormatter.Generator.Extensions;
using Microsoft.CodeAnalysis;

namespace ByteFormatter.Generator.States
{
	public static class ByteFormatterStateProvider
	{
		public static Span<StateGenerationData> GetData(IEnumerable<INamedTypeSymbol> namedTypeSymbols)
		{
			var stateSymbols = namedTypeSymbols.Where(s => s.IsSerializedType()).ToArray();
			if (stateSymbols.Length == 0)
				return Array.Empty<StateGenerationData>();

			var stateCount = 0;
			var stateGenerationsData = new StateGenerationData[stateSymbols.Length];
			var propertiesData = new List<StatePropertyData>();

			foreach (var stateSymbol in stateSymbols)
			{
				propertiesData.Clear();

				foreach (var member in stateSymbol.GetMembers())
				{
					if (member is not IPropertySymbol propertySymbol)
						continue;

					var attributes = propertySymbol.GetAttributes();
					var propertyKeyAttr = attributes
						.FirstOrDefault(s => s.AttributeClass?.Name == TypesLookup.PropertyKeyAttribute);

					if (propertyKeyAttr == null)
						continue;

					var ctorArg = propertyKeyAttr.ConstructorArguments.Single();
					var value = ctorArg.Value;
					propertiesData.Add(new StatePropertyData
					{
						Key = value != null ? (ushort)value : (ushort)0,
						Name = propertySymbol.Name,
						Type = propertySymbol.Type
					});
				}

				stateGenerationsData[stateCount++] = new StateGenerationData
				{
					Namespace = stateSymbol.GetNamespace(),
					Name = stateSymbol.Name,
					Properties = propertiesData.ToArray()
				};
			}

			return new Span<StateGenerationData>(stateGenerationsData, 0, stateCount);
		}
	}
}