﻿using System;
using System.Collections.Generic;

namespace ByteFormatter.Generator.Helpers
{
	public static class AssemblyHelper
	{
		public static IEnumerable<Type> GetTypes()
		{
			var types = new List<Type>();
			var assemblies = AppDomain.CurrentDomain.GetAssemblies();
			foreach (var assembly in assemblies)
			{
				try
				{
					types.AddRange(assembly.GetTypes());
				}
				catch (Exception)
				{
				}
			}

			return types;
		}
	}
}