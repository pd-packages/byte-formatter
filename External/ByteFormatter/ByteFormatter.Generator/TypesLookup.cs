using System;
using System.Linq;
using ByteFormatter.Generator.Helpers;

namespace ByteFormatter.Generator
{
	public struct TypesLookup
	{
		public const string Namespace = "ByteFormatter.Runtime";

		public const string IByteConvertable = "IByteConvertable";
		public const string IByteConvertableFullName = Namespace + ".IByteConvertable";
		public const string ByteWriter = "ByteWriter";
		public const string ByteWriterFull = Namespace + ".ByteWriter";
		public const string ByteReader = "ByteReader";
		public const string ByteReaderFull = Namespace + ".ByteReader";

		public const string ByteExtension = "ByteExtension";
		public const string ByteExtensionAttribute = "ByteExtensionAttribute";
		public const string ByteDataContext = "ByteDataContext";
		public const string ByteDataContextAttribute = "ByteDataContextAttribute";
		public const string PropertyKeyAttribute = "PropertyKeyAttribute";

		public static Type IByteConvertableType { get; } = AssemblyHelper.GetTypes()
			.FirstOrDefault(f => f.Name == IByteConvertable);
	}
}