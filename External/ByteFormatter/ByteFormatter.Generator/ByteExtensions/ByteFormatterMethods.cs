using System.Collections.Generic;
using System.Linq;
using ByteFormatter.Generator.Extensions;
using Microsoft.CodeAnalysis;

namespace ByteFormatter.Generator.ByteExtensions
{
	public static class ByteFormatterMethods
	{
		private static readonly Dictionary<string, ByteConvertMethodData> WriteMethods = new();
		private static readonly Dictionary<string, ByteConvertMethodData> ReadMethods = new();

		public static ByteConvertMethodData GetWrite(string serializableType)
			=> WriteMethods.TryGetValue(serializableType, out var methodData)
				? methodData
				: new ByteConvertMethodData(false, "NoWriteMethod", serializableType);

		public static ByteConvertMethodData GetRead(string serializableType)
			=> ReadMethods.TryGetValue(serializableType, out var methodData)
				? methodData
				: new ByteConvertMethodData(false, "NoReadMethod", serializableType);

		public static void Collect(IEnumerable<INamedTypeSymbol> namedTypeSymbols)
		{
			foreach (var namedTypeSymbol in namedTypeSymbols)
			{
				var attributes = namedTypeSymbol.GetAttributes();
				if (attributes.All(attr => attr.AttributeClass?.Name != TypesLookup.ByteExtensionAttribute))
					continue;

				var members = namedTypeSymbol.GetMembers();
				foreach (var member in members)
				{
					if (member is not IMethodSymbol methodSymbol)
						continue;

					var methodSymbolName = methodSymbol.Name;
					if (methodSymbolName.StartsWith("Write"))
					{
						if (!TryGetWriteType(methodSymbol, out var typeSymbol))
							continue;

						var name = typeSymbol.GetSerializableType();
						if (WriteMethods.ContainsKey(name))
							continue;

						WriteMethods.Add(
							name,
							new ByteConvertMethodData(
								methodSymbol.IsGenericMethod,
								methodSymbolName,
								name
							)
						);
					}
					else if (methodSymbolName.StartsWith("Read"))
					{
						if (!TryGetReadType(methodSymbol, out var typeSymbol))
							continue;

						var name = typeSymbol.GetSerializableType();
						if (ReadMethods.ContainsKey(name))
							continue;

						ReadMethods.Add(
							name,
							new ByteConvertMethodData(
								methodSymbol.IsGenericMethod,
								methodSymbolName,
								name
							)
						);
					}
				}
			}
		}

		private static bool TryGetWriteType(IMethodSymbol methodSymbol, out ITypeSymbol typeSymbol)
		{
			typeSymbol = null;
			IParameterSymbol parameterSymbol;
			var parameters = methodSymbol.Parameters;
			if (methodSymbol.IsStatic)
			{
				if (parameters.Length != 2)
					return false;

				if (parameters[0].Type.Name != TypesLookup.ByteWriter)
					return false;

				parameterSymbol = parameters[1];
			}
			else
			{
				if (parameters.Length != 1)
					return false;

				parameterSymbol = parameters[0];
			}

			typeSymbol = parameterSymbol.Type;
			return true;
		}

		private static bool TryGetReadType(IMethodSymbol methodSymbol, out ITypeSymbol typeSymbol)
		{
			typeSymbol = null;
			var parameters = methodSymbol.Parameters;
			if (methodSymbol.IsStatic)
			{
				if (parameters.Length != 1)
					return false;

				if (parameters[0].Type.Name != TypesLookup.ByteReader)
					return false;
			}
			else
			{
				if (parameters.Length > 0)
					return false;
			}

			typeSymbol = methodSymbol.ReturnType;
			return true;
		}
	}
}