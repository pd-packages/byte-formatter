using System;
using System.Linq;
using ByteFormatter.Generator.Extensions;
using Microsoft.CodeAnalysis;

namespace ByteFormatter.Generator.ByteExtensions
{
	public class ByteConvertMethodData
	{
		public readonly bool IsGeneric;
		public readonly string MethodName;
		public readonly string FullName;

		public ByteConvertMethodData(
			bool isGeneric,
			string methodName,
			string fullName
		)
		{
			IsGeneric = isGeneric;
			MethodName = methodName;
			FullName = fullName.Split(new[] { '`' }, StringSplitOptions.RemoveEmptyEntries)[0];
		}

		public string GetMethod(ITypeSymbol typeSymbol)
		{
			if (!IsGeneric)
				return MethodName;

			return typeSymbol switch
			{
				IArrayTypeSymbol arrayTypeSymbol => $"{MethodName}<{arrayTypeSymbol.ElementType.GetFullName()}>",
				INamedTypeSymbol namedTypeSymbol => namedTypeSymbol.IsGenericType
					? namedTypeSymbol.Name == "Nullable"
						? $"{MethodName}<{namedTypeSymbol.TypeArguments.Single().GetFullName()}>"
						: $"{MethodName}<{string.Join(", ", namedTypeSymbol.TypeArguments.Select(s => s.GetFullName()))}>"
					: $"{MethodName}<{namedTypeSymbol.GetFullName()}>",
				_ => throw new Exception(
					$"[{nameof(ByteConvertMethodData)}] Cannot process type symbol: {typeSymbol.GetSerializableType()}")
			};
		}

		protected bool Equals(ByteConvertMethodData other) => FullName == other.FullName;

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((ByteConvertMethodData)obj);
		}

		public override int GetHashCode() => (FullName != null ? FullName.GetHashCode() : 0);
	}
}