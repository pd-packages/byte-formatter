using System;
using System.Text;
using ByteFormatter.Generator.ByteExtensions;
using ByteFormatter.Generator.Extensions;
using ByteFormatter.Generator.States;

namespace ByteFormatter.Generator.Generators
{
	public static class ByteFormatterGenerator
	{
		public static GeneratedFile[] Generate(Span<StateGenerationData> statesData)
		{
			var generatedFiles = new GeneratedFile[statesData.Length + 1];

			for (var i = 0; i < statesData.Length; i++)
			{
				try
				{
					var stateData = statesData[i];
					generatedFiles[i] = new GeneratedFile(
						stateData.GetFileName(),
						ByteFormatterTpl.ByteConvertableFile(
							stateData.Namespace,
							stateData.Name,
							GetWriteMethodBody(stateData),
							GetReadMethodBody(stateData)
						)
					);
				}
				catch (Exception e)
				{
				}
			}

			generatedFiles[generatedFiles.Length - 1] = ByteFormatterInitializerGenerator.Generate(statesData);
			return generatedFiles;
		}

		private static string GetWriteMethodBody(StateGenerationData stateData)
		{
			var builder = new StringBuilder();
			builder.Append(ByteFormatterTpl.WriteObjectMap(stateData.Properties.Length));

			for (var i = 0; i < stateData.Properties.Length; i++)
			{
				try
				{
					var propertyData = stateData.Properties[i];
					builder.Append('\n')
						.Append(ByteFormatterTpl.WritePropertyKey(propertyData.Key));
				}
				catch (Exception)
				{
				}
			}

			for (var i = 0; i < stateData.Properties.Length; i++)
			{
				try
				{
					var propertyData = stateData.Properties[i];
					builder.Append('\n')
						.Append(ByteFormatterTpl.WriteProperty(propertyData.Name, i));
				}
				catch (Exception e)
				{
				}
			}

			return builder.ToString();
		}

		private static string GetReadMethodBody(StateGenerationData stateData)
		{
			var builder = new StringBuilder();
			builder.Append(ByteFormatterTpl.BeginObjectRead());

			for (var i = 0; i < stateData.Properties.Length; i++)
			{
				try
				{
					var propertyData = stateData.Properties[i];
					var methodData = ByteFormatterMethods.GetRead(propertyData.Type.GetSerializableType());
					builder.Append('\n').Append(ByteFormatterTpl.ReadProperty(
						propertyData.Name,
						propertyData.Key,
						methodData.GetMethod(propertyData.Type)
					));
				}
				catch (Exception)
				{
				}
			}

			builder.Append('\n').Append(ByteFormatterTpl.EndObjectRead());
			return builder.ToString();
		}
	}
}