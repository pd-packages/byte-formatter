using System;
using System.Collections.Generic;
using System.Text;
using ByteFormatter.Generator.ByteExtensions;
using ByteFormatter.Generator.Extensions;
using ByteFormatter.Generator.States;
using Microsoft.CodeAnalysis;

namespace ByteFormatter.Generator.Generators
{
	public class ByteFormatterInitializerGenerator
	{
		public static GeneratedFile Generate(Span<StateGenerationData> statesData)
		{
			var typeSet = new HashSet<string>();
			var builder = new StringBuilder();

			foreach (var stateData in statesData)
			foreach (var propertyData in stateData.Properties)
				CreateTypeInitialize(typeSet, builder, propertyData.Type);

			var file = ByteFormatterTpl.InitializationFile(builder.ToString());
			return new GeneratedFile("ByteFormatterTypeCache", file);
		}

		private static void CreateTypeInitialize(
			HashSet<string> typeSet,
			StringBuilder builder,
			ITypeSymbol typeSymbol
		)
		{
			if (typeSymbol is INamedTypeSymbol { IsGenericType: true } namedTypeSymbol)
			{
				var immutableArray = namedTypeSymbol.TypeArguments;
				foreach (var symbol in immutableArray)
					CreateTypeInitialize(typeSet, builder, symbol);
			}

			var fullName = typeSymbol.GetFullName();
			if (!typeSet.Add(fullName))
				return;

			var methodData = ByteFormatterMethods.GetRead(typeSymbol.GetSerializableType());
			var readMethod = methodData.GetMethod(typeSymbol);
			builder.Append('\n').Append(ByteFormatterTpl.ByteTypeCache(fullName, readMethod));
		}
	}
}