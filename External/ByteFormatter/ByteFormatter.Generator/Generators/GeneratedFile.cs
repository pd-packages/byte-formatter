namespace ByteFormatter.Generator.Generators
{
	public class GeneratedFile
	{
		private const string EXTENSION = ".g.cs";

		private readonly string _name;
		public readonly string Content;

		public string Name => _name + EXTENSION;

		public GeneratedFile(string name, string content)
		{
			_name = name;
			Content = content;
		}
	}
}