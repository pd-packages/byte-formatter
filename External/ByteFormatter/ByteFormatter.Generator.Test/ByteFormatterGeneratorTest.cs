using ByteFormatter.Runtime.StateSerialize;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace ByteFormatter.Generator.Test;

public class ByteFormatterGeneratorTest
{
	private static readonly string State1 =
		$$"""

		  using {{typeof(List<>).Namespace}};
		  using {{typeof(Dictionary<,>).Namespace}};
		  using {{typeof(ByteDataContextAttribute).Namespace}};

		  [{{nameof(ByteDataContextAttribute)}}(nameof(State1))]
		  public partial class State1
		  {
		      [{{nameof(PropertyKeyAttribute)}}(1)] public int Value1 { get; set; }
		      [{{nameof(PropertyKeyAttribute)}}(2)] public string Value2 { get; set; }
		      [{{nameof(PropertyKeyAttribute)}}(3)] public string[] Value3 { get; set; }
		      [{{nameof(PropertyKeyAttribute)}}(4)] public StateSub Value4 { get; set; }
		      [{{nameof(PropertyKeyAttribute)}}(5)] public List<StateSub> Value5 { get; set; }
		      [{{nameof(PropertyKeyAttribute)}}(6)] public Dictionary<int, StateSub> Value6 { get; set; }
		      [{{nameof(PropertyKeyAttribute)}}(7)] public int? Value7 { get; set; }
		      [{{nameof(PropertyKeyAttribute)}}(8)] public long? Value8 { get; set; }
		      [{{nameof(PropertyKeyAttribute)}}(9)] public long?[] Value9 { get; set; }
		      [{{nameof(PropertyKeyAttribute)}}(10)] public long[] Value10 { get; set; }
		      
		      public NotSerializedData Data { get; set; }
		  }

		  public class NotSerializedData
		  {
		  }    
		                                        
		  """;

	private static readonly string StateSub =
		$$"""

		  using {{typeof(ByteDataContextAttribute).Namespace}};

		  [{{nameof(ByteDataContextAttribute)}}(nameof(StateSub))]
		  public partial class StateSub
		  {
		      [{{nameof(PropertyKeyAttribute)}}(1)] public int Value1 { get; set; }
		      [{{nameof(PropertyKeyAttribute)}}(2)] public string Value2 { get; set; }
		  }    
		                                        
		  """;

	private static readonly string State2 =
		$$"""

		  namespace MyProject.Scripts
		  {
		  	[{{typeof(ByteDataContextAttribute).FullName}}(nameof(State2))]
		  	public partial class State2
		  	{
		  		[{{typeof(PropertyKeyAttribute).FullName}}(1)] public int Value1 { get; set; }
		  		[{{typeof(PropertyKeyAttribute).FullName}}(2)] public string Value2 { get; set; }
		  	}
		  }

		  """;

	private static readonly string CustomTypeSerialization =
		$$"""

		  using {{typeof(ByteDataContextAttribute).Namespace}};

		  public enum CustomEnum
		  {
		      One,
		      Two
		  }

		  [{{nameof(ByteExtensionAttribute)}}]
		  public static class ByteFormatterExtensions
		  {
		  
		      public static void Write(this ByteWriter writer, CustomEnum value)
		          => writer.Write((byte) value);
		  
		      public static CustomEnum ReadCustomEnum(this ByteReader reader)
		          => (CustomEnum) reader.ReadInt32();
		  }

		  [{{typeof(ByteDataContextAttribute).FullName}}(nameof(CustomEnumState))]
		  public partial class CustomEnumState
		  {
		      [{{typeof(PropertyKeyAttribute).FullName}}(1)] public CustomEnum Value1 { get; set; }
		      [{{typeof(PropertyKeyAttribute).FullName}}(2)] public CustomEnum? Value2 { get; set; }
		  }

		  """;

	[Fact]
	public void Generate()
	{
		// Create an instance of the source generator.
		var generator = new IncrementalGenerator();

		// Source generators should be tested using 'GeneratorDriver'.
		var driver = CSharpGeneratorDriver.Create(generator);

		var referencePaths = new[]
		{
			typeof(ByteDataContextAttribute).Assembly.Location,
			typeof(object).Assembly.Location, // mscorlib.dll or System.Private.CoreLib.dll
			Path.Combine(Path.GetDirectoryName(typeof(object).Assembly.Location)!, "System.Runtime.dll"),
			Path.Combine(Path.GetDirectoryName(typeof(object).Assembly.Location)!, "System.Console.dll"),
			Path.Combine(Path.GetDirectoryName(typeof(object).Assembly.Location)!, "netstandard.dll"),
		};

		// We need to create a compilation with the required source code.
		var compilation = CSharpCompilation.Create(nameof(ByteFormatterGeneratorTest),
			new[]
			{
				CSharpSyntaxTree.ParseText(State1),
				CSharpSyntaxTree.ParseText(State2),
				CSharpSyntaxTree.ParseText(StateSub),
				CSharpSyntaxTree.ParseText(CustomTypeSerialization),
			},
			referencePaths.Select(path => MetadataReference.CreateFromFile(path))
		);

		// Run generators and retrieve all results.
		var runResult = driver.RunGenerators(compilation).GetRunResult();
		var generatedTrees = runResult.GeneratedTrees;

		foreach (var result in runResult.Results)
			Assert.Null(result.Exception);
	}
}