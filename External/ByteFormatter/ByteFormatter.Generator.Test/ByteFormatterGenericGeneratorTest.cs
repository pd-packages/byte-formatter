using ByteFormatter.Runtime.StateSerialize;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace ByteFormatter.Generator.Test;

public class ByteFormatterGenericGeneratorTest
{
	private static readonly string State1 =
		$$"""

		  using {{typeof(Dictionary<,>).Namespace}};
		  using {{typeof(ByteDataContextAttribute).Namespace}};

		  [{{nameof(ByteDataContextAttribute)}}(nameof(State1))]
		  public partial class State1
		  {
		      [{{nameof(PropertyKeyAttribute)}}(1)] public Dictionary<int, StateSub> Value1 { get; set; }
		  }
		                                        
		  """;

	private static readonly string StateSub =
		$$"""

		  using {{typeof(ByteDataContextAttribute).Namespace}};

		  [{{nameof(ByteDataContextAttribute)}}(nameof(StateSub))]
		  public partial class StateSub
		  {
		      [{{nameof(PropertyKeyAttribute)}}(1)] public string Value1 { get; set; }
		  }    
		                                        
		  """;

	[Fact]
	public void Generate()
	{
		// Create an instance of the source generator.
		var generator = new IncrementalGenerator();

		// Source generators should be tested using 'GeneratorDriver'.
		var driver = CSharpGeneratorDriver.Create(generator);

		var referencePaths = new[]
		{
			typeof(ByteDataContextAttribute).Assembly.Location,
			typeof(object).Assembly.Location, // mscorlib.dll or System.Private.CoreLib.dll
			Path.Combine(Path.GetDirectoryName(typeof(object).Assembly.Location)!, "System.Runtime.dll"),
			Path.Combine(Path.GetDirectoryName(typeof(object).Assembly.Location)!, "System.Console.dll"),
			Path.Combine(Path.GetDirectoryName(typeof(object).Assembly.Location)!, "netstandard.dll"),
		};

		// We need to create a compilation with the required source code.
		var compilation = CSharpCompilation.Create(nameof(ByteFormatterGeneratorTest),
			new[]
			{
				CSharpSyntaxTree.ParseText(State1),
				CSharpSyntaxTree.ParseText(StateSub),
			},
			referencePaths.Select(path => MetadataReference.CreateFromFile(path))
		);

		// Run generators and retrieve all results.
		var runResult = driver.RunGenerators(compilation).GetRunResult();
		var generatedTrees = runResult.GeneratedTrees;

		foreach (var result in runResult.Results)
			Assert.Null(result.Exception);
	}
}