# Byte Formatter

[![NPM package](https://img.shields.io/npm/v/com.playdarium.unity.byte-formatter?logo=npm&logoColor=fff&label=NPM+package&color=limegreen)](https://www.npmjs.com/package/com.playdarium.unity.byte-formatter)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Installing

Using the native Unity Package Manager introduced in 2017.2, you can add this library as a package by modifying your
`manifest.json` file found at `/ProjectName/Packages/manifest.json` to include it as a dependency. See the example below
on how to reference it.

### Install via OpenUPM

The package is available on the [npmjs](https://www.npmjs.com/package/com.playdarium.unity.byte-formatter/)
registry.

#### Add registry scope

```
{
  "dependencies": {
    ...
    "com.playdarium.unity.byte-formatter": "x.x.x",
    ...
  },
  "scopedRegistries": [
    {
      "name": "Playdarium",
      "url": "https://registry.npmjs.org",
      "scopes": [
        "com.playdarium.unity"
      ]
    }
  ]
}
```

#### Add package in PackageManager

Open `Window -> Package Manager` choose `Packages: My Regestries` and install package

### Install via GIT URL

```
"com.playdarium.unity.byte-formatter": "https://gitlab.com/pd-packages/byte-formatter.git#upm"
```

# Introduction

Byte formatter has different pices:

- [ByteReader and ByteWriter](#ByteReader-and-ByteWriter)
- [State serialization](#State-serialization)
- [Byte data validator](#Byte-data-validator)
- [State serialization principle](#State-serialization-principle)

# ByteReader and ByteWriter

Write data example:

```c#
public byte[] WriteBytesFromState(State state)
{
    var writer = new ByteWriter();
    writer.Write(state.Value1);
    writer.Write(state.Value2);
    return writer.ToArray();
}
```

The resulting array of bytes will contain data written in a strict sequence.
Data reading should be carried out in the same sequence in which they were written.

Read data example:

```c#
public State WriteBytesFromState(byte[] bytes)
{
    var state = new State();
    var reader = new ByteReader(bytes);
    state.Value1 = reader.ReadInt32();
    state.Value2 = reader.ReadInt32();
    return state;
}
```

# State serialization

### Prepare classes for generation

Serialized class must be `public partial` and has attribute `[ByteDataContext(nameof(SerializedClass))]`

The data to be serialized must be properties with an attribute `[PropertyKey(ushort key)]` where key is start from `1`

Serialized class example:

```c#
[ByteDataContext(nameof(State))]
public partial class State
{
    [PropertyKey(1)] public int Value1 { get; set; }
    [PropertyKey(2)] public string Value2 { get; set; }
}
```

After this you can validate writing and reading data `Tools/StateSerialize/Validate Serialization`

### Warning!

If data added or removed or indexes number changed for serialization, you need to create migration

### Add custom data writing and reading

Create static class and add specific attribute `[ByteExtension]` for custom extensions.

Example:

```c#
[ByteExtension]
public static class CustomByteFormatterExtensions{
    ...
}
```

Custom read or write extensions **do not** support generic types for generation.

#### Write extension

Method name must in start contains `Write` word and as argument you custom type.

Example:

```c#
public static void WriteMyCustomType(this ByteWriter writer, MyCustomType value)
{
    writer.Write((int) value);
}
```

#### Read extension

Method name must contains `Read` word and no any arguments.

Example:

```c#
public static MyCustomType ReadMyCustomType(this ByteReader reader)
{
    return (MyCustomType) reader.ReadInt32();
}

public static void SkipMyCustomType(this ByteReader reader){
    reader.SkipInt32();
}
```

# Byte data validator

For data validation you must create class with attribute `[ByteValidatorDataProvider]` in Editor folder.
Class must provide custom data variants.

Where:

- DataArray - custom data.
- DataSizeArray - data size in bytes.

```c#
[ByteValidatorDataProvider]
public class MyCustomTypeDataProvider : ADataProvider<MyCustomType>
{
	protected override MyCustomType[] DataArray { get; } = {(MyCustomType) 457};
	protected override int[] DataSizeArray { get; } = {4};
}
```

# State serialization principle

### Object mapping

**Example state:**

```c#
[ByteDataContext(nameof(State))]
public partial class State
{
    [PropertyKey(1)] public int Value1 { get; set; }
    [PropertyKey(2)] public int Value2 { get; set; }
}
```

This is the object map of example state

| Name             | Type   | Data   | Size   |
|------------------|--------|--------|--------|
| Properties Count | ushort | 2      | 2 byte |
| Key 1            | ushort | 1      | 2 byte |
| Offset 1         | int    | 0      | 4 byte |
| Key 2            | ushort | 2      | 2 byte |
| Offset 2         | int    | 4      | 4 byte |
| Data 1           | int    | Value1 | 4 byte |
| Data 2           | int    | Value2 | 4 byte |