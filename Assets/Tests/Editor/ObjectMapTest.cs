using ByteFormatter.Runtime;
using NUnit.Framework;

namespace Tests.Editor
{
	public class ObjectMapTest
	{
		[Test]
		public void WriteAndRead()
		{
			// Begin object write
			var writer = new ByteWriter();
			var objectMapWriter = new ObjectMapWriter(writer, 3);

			// Write object field indexes
			objectMapWriter.WritePropertyKey(1);
			objectMapWriter.WritePropertyKey(2);
			objectMapWriter.WritePropertyKey(3);

			// Write object data
			writer.Write(123L);
			objectMapWriter.WritePropertyOffset(0);

			writer.Write(456L);
			objectMapWriter.WritePropertyOffset(1);

			writer.Write(789L);
			objectMapWriter.WritePropertyOffset(2);

			var array = writer.ToArray();
			Assert.AreEqual(2 + 18 + 24, array.Length);

			var readedValues = new long[5];
			var reader = new ByteReader(array);
			var objectMapReader = new ObjectMapReader(reader);

			Assert.True(objectMapReader.TryReadProperty(1));
			readedValues[0] = reader.ReadInt64();

			Assert.True(objectMapReader.TryReadProperty(2));
			readedValues[1] = reader.ReadInt64();

			Assert.True(objectMapReader.TryReadProperty(3));
			readedValues[2] = reader.ReadInt64();

			Assert.False(objectMapReader.TryReadProperty(0));
			Assert.False(objectMapReader.TryReadProperty(4));

			objectMapReader.EndReadObject();

			Assert.AreEqual(writer.Position, reader.Position);
			Assert.AreEqual(123L, readedValues[0]);
			Assert.AreEqual(456L, readedValues[1]);
			Assert.AreEqual(789L, readedValues[2]);
			Assert.AreEqual(0L, readedValues[3]);
			Assert.AreEqual(0L, readedValues[4]);
		}
	}
}