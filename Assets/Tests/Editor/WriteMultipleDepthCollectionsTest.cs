using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ByteFormatter.Runtime;
using ByteFormatter.Runtime.StateSerialize;
using DefaultNamespace.Tests.Models;
using NUnit.Framework;
using Random = UnityEngine.Random;

namespace Tests.Editor
{
	[ByteDataContext(nameof(ListInListState))]
	public partial class ListInListState
	{
		[PropertyKey(1)] public List<List<int>> Collection { get; set; }
	}

	[ByteDataContext(nameof(DictionaryInDictionaryState))]
	public partial class DictionaryInDictionaryState
	{
		[PropertyKey(1)] public Dictionary<Guid, Dictionary<Guid, SubModelA>> Collection { get; set; }
	}

	public class WriteMultipleDepthCollectionsTest
	{
		[SetUp]
		public void SetUp()
		{
			ByteFormatterTypeCache.Init();
		}

		[Test]
		public void ListInList_WriteAndRead()
		{
			var expected = new ListInListState
			{
				Collection = new List<List<int>>(
					Enumerable.Repeat(new List<int>(
						Enumerable.Range(1, 10)
					), 10)
				)
			};

			var writer = new ByteWriter();
			expected.ToByte(writer);

			var reader = new ByteReader(writer.ToArray());
			var actual = reader.ReadByteConvertable<ListInListState>();

			Assert.AreEqual(expected.Collection.Count, actual.Collection.Count);

			var expectedEnumerator = expected.Collection.GetEnumerator();
			var actualEnumerator = actual.Collection.GetEnumerator();

			while (expectedEnumerator.MoveNext() && actualEnumerator.MoveNext())
			{
				var expectedInnerCollection = expectedEnumerator.Current;
				var actualInnerCollection = actualEnumerator.Current;

				Assert.True(expectedInnerCollection.SequenceEqual(actualInnerCollection));
			}
		}

		[Test]
		public void DictionaryInDictionary_WriteAndRead()
		{
			var expected = new DictionaryInDictionaryState()
			{
				Collection = new Dictionary<Guid, Dictionary<Guid, SubModelA>>(
					Enumerable.Range(1, 10).Select(_ => new KeyValuePair<Guid, Dictionary<Guid, SubModelA>>(
						Guid.NewGuid(), new Dictionary<Guid, SubModelA>(
							Enumerable.Range(1, 10).Select(_ => new KeyValuePair<Guid, SubModelA>(
								Guid.NewGuid(), new SubModelA
								{
									StringValue = GenerateRandomString(10),
									LongValue = Random.Range(int.MinValue, int.MaxValue)
								}
							))
						)
					))
				)
			};

			var writer = new ByteWriter();
			expected.ToByte(writer);

			var reader = new ByteReader(writer.ToArray());
			var actual = reader.ReadByteConvertable<DictionaryInDictionaryState>();

			Assert.AreEqual(expected.Collection.Count, actual.Collection.Count);

			var expectedEnumerator = expected.Collection.GetEnumerator();
			var actualEnumerator = actual.Collection.GetEnumerator();

			while (expectedEnumerator.MoveNext() && actualEnumerator.MoveNext())
			{
				var expectedInnerCollection = expectedEnumerator.Current;
				var actualInnerCollection = actualEnumerator.Current;

				Assert.AreEqual(expectedInnerCollection.Key, actualInnerCollection.Key);
				Assert.True(expectedInnerCollection.Value.Values.SequenceEqual(actualInnerCollection.Value.Values));
			}
		}

		public static string GenerateRandomString(int length)
		{
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			var stringBuilder = new StringBuilder(length);

			for (var i = 0; i < length; i++)
			{
				var randomChar = chars[Random.Range(0, chars.Length)];
				stringBuilder.Append(randomChar);
			}

			return stringBuilder.ToString();
		}
	}
}