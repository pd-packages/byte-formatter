using System;
using ByteFormatter.Runtime;
using DefaultNamespace.Tests.Models;
using NUnit.Framework;

namespace Tests.Editor
{
	public class WriteReadTest
	{
		private ModelA _modelA;

		[SetUp]
		public void SetUp()
		{
			ByteFormatterTypeCache.Init();
			_modelA = ModelA.CreateRandom();
		}

		[Test]
		public void SerializeAndDeserialize_Empty_Object()
		{
			var emptyModel = new EmptyModel();
			var writer = new ByteWriter();
			writer.Write(emptyModel);
			var reader = new ByteReader(writer.ToArray());
			var model = reader.ReadByteConvertable<EmptyModel>();
		}

		[Test]
		public void SerializeAndDeserialize_Models_AreEqual()
		{
			var writer = new ByteWriter();

			_modelA.ToByte(writer);

			var reader = new ByteReader(writer.ToArray());
			var deserializedModelA = reader.ReadByteConvertable<ModelA>();

			Assert.AreEqual(_modelA, deserializedModelA);
		}

		[Test]
		public void Read_Serialized_ModelA_As_ModelB()
		{
			var writer = new ByteWriter();
			_modelA.ToByte(writer);

			var reader = new ByteReader(writer.ToArray());
			var modelB = reader.ReadByteConvertable<ModelB>();

			Assert.True(
				modelB.StringValue.Equals(_modelA.StringValue),
				"modelB.StringValue.Equals(_modelA.StringValue)"
			);
			Assert.True(
				modelB.SubModelAValue.Equals(_modelA.SubModelAValue),
				"modelB.SubModelAValue.Equals(_modelA.SubModelAValue)"
			);
			Assert.True(
				modelB.IntByGuid.SequenceEqual(_modelA.IntByGuid),
				"modelB.IntByGuid.SequenceEqual(_modelA.IntByGuid)"
			);
			Assert.True(
				modelB.SubModelAByGuid.SequenceEqual(_modelA.SubModelAByGuid),
				"modelB.SubModelAByGuid.SequenceEqual(_modelA.SubModelAByGuid)"
			);
		}

		[Test]
		public void Read_Serialized_ModelA_As_ModelC()
		{
			var writer = new ByteWriter();
			_modelA.ToByte(writer);

			var reader = new ByteReader(writer.ToArray());
			var modelC = reader.ReadByteConvertable<ModelC>();

			Assert.True(
				modelC.StringValue.Equals(_modelA.StringValue),
				"modelB.StringValue.Equals(_modelA.StringValue)"
			);
			Assert.True(
				modelC.SubModelAValue.Equals(_modelA.SubModelAValue),
				"modelB.SubModelAValue.Equals(_modelA.SubModelAValue)"
			);
			Assert.True(
				modelC.IntByGuid.SequenceEqual(_modelA.IntByGuid),
				"modelB.IntByGuid.SequenceEqual(_modelA.IntByGuid)"
			);
			Assert.True(
				modelC.SubModelAByGuid.SequenceEqual(_modelA.SubModelAByGuid),
				"modelB.SubModelAByGuid.SequenceEqual(_modelA.SubModelAByGuid)"
			);
			Assert.Null(modelC.IntList2);
		}

		[Test]
		public void Read_Serialized_ModelA_As_AnotherModel()
		{
			var writer = new ByteWriter();
			_modelA.ToByte(writer);

			var reader = new ByteReader(writer.ToArray());
			var anotherModel = reader.ReadByteConvertable<AnotherModel>();
			Assert.AreEqual(Guid.Empty, anotherModel.GuidValue);
			Assert.Zero(anotherModel.DoubleValue);
		}
	}
}