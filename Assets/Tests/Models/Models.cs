using System;
using System.Collections.Generic;
using System.Linq;
using ByteFormatter.Runtime.StateSerialize;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DefaultNamespace.Tests.Models
{
	[ByteDataContext(nameof(EmptyModel))]
	public partial class EmptyModel
	{
	}

	[ByteDataContext(nameof(ModelA))]
	public partial class ModelA
	{
		[PropertyKey(1)] public string StringValue { get; set; }
		[PropertyKey(2)] public long LongValue { get; set; }
		[PropertyKey(3)] public SubModelA SubModelAValue { get; set; }
		[PropertyKey(4)] public List<int> IntList { get; set; }
		[PropertyKey(5)] public Dictionary<Guid, int> IntByGuid { get; set; }
		[PropertyKey(6)] public Dictionary<Guid, SubModelA> SubModelAByGuid { get; set; }
		[PropertyKey(8)] public Vector3? Geo { get; set; }
		[PropertyKey(9)] public Vector3?[] GeoHistory { get; set; }

		protected bool Equals(ModelA other)
			=> StringValue == other.StringValue
			   && LongValue == other.LongValue
			   && Equals(SubModelAValue, other.SubModelAValue)
			   && IntList.SequenceEqual(IntList)
			   && IntByGuid.SequenceEqual(other.IntByGuid)
			   && SubModelAByGuid.SequenceEqual(other.SubModelAByGuid)
			   && Geo.Equals(other.Geo)
			   && GeoHistory.SequenceEqual(other.GeoHistory);

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((ModelA)obj);
		}

		public override int GetHashCode()
			=> HashCode.Combine(StringValue, LongValue, SubModelAValue, IntList, IntByGuid, SubModelAByGuid);

		public static ModelA CreateRandom() => new()
		{
			StringValue = Guid.NewGuid().ToString(),
			LongValue = Random.Range(int.MinValue, int.MaxValue),
			SubModelAValue = SubModelA.CreateRandom(),
			IntList = Enumerable.Range(1, 16).ToList(),
			IntByGuid = Enumerable.Range(1, 16).ToDictionary(_ => Guid.NewGuid()),
			SubModelAByGuid = Enumerable.Range(1, 16)
				.ToDictionary(_ => Guid.NewGuid(), _ => SubModelA.CreateRandom()),
			Geo = Random.insideUnitSphere,
			GeoHistory = Enumerable.Range(1, 10).Select(_ => Random.insideUnitSphere as Vector3?).ToArray()
		};
	}

	[ByteDataContext(nameof(ModelB))]
	public partial class ModelB
	{
		[PropertyKey(1)] public string StringValue { get; set; }
		[PropertyKey(3)] public SubModelA SubModelAValue { get; set; }
		[PropertyKey(5)] public Dictionary<Guid, int> IntByGuid { get; set; }
		[PropertyKey(6)] public Dictionary<Guid, SubModelA> SubModelAByGuid { get; set; }
	}

	[ByteDataContext(nameof(ModelC))]
	public partial class ModelC
	{
		[PropertyKey(1)] public string StringValue { get; set; }
		[PropertyKey(3)] public SubModelA SubModelAValue { get; set; }
		[PropertyKey(5)] public Dictionary<Guid, int> IntByGuid { get; set; }
		[PropertyKey(6)] public Dictionary<Guid, SubModelA> SubModelAByGuid { get; set; }
		[PropertyKey(7)] public List<int> IntList2 { get; set; }
	}

	[ByteDataContext(nameof(AnotherModel))]
	public partial class AnotherModel
	{
		[PropertyKey(11)] public Guid GuidValue { get; set; }
		[PropertyKey(12)] public double DoubleValue { get; set; }
	}

	[ByteDataContext(nameof(SubModelA))]
	public partial class SubModelA
	{
		[PropertyKey(1)] public string StringValue { get; set; }
		[PropertyKey(2)] public long LongValue { get; set; }

		protected bool Equals(SubModelA other) => StringValue == other.StringValue && LongValue == other.LongValue;

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((SubModelA)obj);
		}

		public override int GetHashCode() => HashCode.Combine(StringValue, LongValue);

		public static SubModelA CreateRandom() => new()
		{
			StringValue = Guid.NewGuid().ToString(),
			LongValue = Random.Range(int.MinValue, int.MaxValue)
		};
	}

	public static class LinqExtensions
	{
		public static bool SequenceEqual<TKey, TValue>(
			this IDictionary<TKey, TValue> first,
			IDictionary<TKey, TValue> second
		)
			=> first.Keys.SequenceEqual(second.Keys) && first.Values.SequenceEqual(second.Values);
	}
}