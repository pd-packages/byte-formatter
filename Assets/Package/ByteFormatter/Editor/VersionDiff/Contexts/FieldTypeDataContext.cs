﻿using System.Collections.Generic;
using ByteFormatter.Runtime.MigrateHelper;
using Package.ByteFormatter.Generator;

namespace Package.ByteFormatter.VersionDiff.Contexts
{
	public class FieldTypeDataContext : ADataContext
	{
		private static readonly List<ADataContext> Contexts = new List<ADataContext>();

		public override string Name { get; }
		public override List<ADataContext> ChildContexts => Contexts;

		public FieldTypeDataContext(ByteDataTypeAccessAttribute attribute)
		{
			Name = attribute.Type.GetFriendlyName();
		}
	}
}