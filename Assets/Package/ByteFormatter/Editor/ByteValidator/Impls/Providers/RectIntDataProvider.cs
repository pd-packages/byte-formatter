﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class RectIntDataProvider : ADataProvider<RectInt>
	{
		protected override RectInt[] DataArray { get; } = {new RectInt(1, 1, 1, 1)};
		protected override int[] DaraSizeArray { get; } = {16};
	}
}