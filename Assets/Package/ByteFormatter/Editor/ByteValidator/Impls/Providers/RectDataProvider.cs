﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class RectDataProvider : ADataProvider<Rect>
	{
		protected override Rect[] DataArray { get; } = {new Rect(1, 1, 1, 1)};
		protected override int[] DaraSizeArray { get; } = {16};
	}
}