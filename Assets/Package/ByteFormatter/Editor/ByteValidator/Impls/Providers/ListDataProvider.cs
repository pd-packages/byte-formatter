﻿using System;
using System.Collections;
using System.Collections.Generic;
using ByteFormatter.Runtime.Utils;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class ListDataProvider : AIListDataProvider
	{
		public override bool CanProvide(Type type) => type.IsGenericTypeDefinition(typeof(List<>));

		protected override Type GetElementType(Type type) => type.GetGenericArguments()[0];

		protected override IList CreateInstance(Type type, int lenght)
		{
			var stub = DataProviderService.CreateValue(0, type);
			var instance = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(type));
			for (var i = 0; i < lenght; i++)
			{
				instance.Add(stub);
			}

			return instance;
		}
	}
}