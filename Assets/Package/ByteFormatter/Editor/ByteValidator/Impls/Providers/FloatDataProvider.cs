﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class FloatDataProvider : ADataProvider<float>
	{
		protected override float[] DataArray { get; } = {float.MaxValue};
		protected override int[] DaraSizeArray { get; } = {4};
	}
}