﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class ByteDataProvider : ADataProvider<byte>
	{
		protected override byte[] DataArray { get; } = { byte.MaxValue };
		protected override int[] DaraSizeArray { get; } = { 1 };
	}
}