using System;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class GuidDataProvider : ADataProvider<Guid>
	{
		protected override Guid[] DataArray { get; } = { Guid.NewGuid() };
		protected override int[] DaraSizeArray { get; } = { 16 };
	}
}