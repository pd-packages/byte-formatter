﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class LongDataProvider : ADataProvider<long>
	{
		protected override long[] DataArray { get; } = {long.MaxValue};
		protected override int[] DaraSizeArray { get; } = {8};
	}
}