﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public abstract class ADataProvider<T> : IDataProvider
	{
		protected abstract T[] DataArray { get; }

		protected abstract int[] DaraSizeArray { get; }

		public int GetValueVariants(Type type) => DataArray.Length;

		public virtual bool CanProvide(Type type) => type == typeof(T);

		public int GetValueSize(int variantIndex, Type type) => DaraSizeArray[variantIndex];

		public object CreateValue(int variantIndex, Type type) => DataArray[variantIndex];

		public void SetValue(int variantIndex, object obj, PropertyInfo propertyInfo)
			=> propertyInfo.SetValue(obj, DataArray[variantIndex]);

		public virtual List<string> AssertValueEquals(int variantIndex, object obj, Type type)
		{
			if (DataArray[variantIndex].Equals(obj))
				return new List<string>();
			return new List<string>
			{
				$"{type.FullName} in object {obj.GetType().Name} not valid"
			};
		}
	}
}