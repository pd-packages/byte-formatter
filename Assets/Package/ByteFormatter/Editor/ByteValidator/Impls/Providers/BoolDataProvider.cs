﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class BoolDataProvider : ADataProvider<bool>
	{
		protected override bool[] DataArray { get; } = {true};
		protected override int[] DaraSizeArray { get; } = {1};
	}
}