﻿namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class UShortDataProvider : ADataProvider<ushort>
	{
		protected override ushort[] DataArray { get; } = { ushort.MaxValue };
		protected override int[] DaraSizeArray { get; } = { 2 };
	}
}