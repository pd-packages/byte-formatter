﻿using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class Vector3DataProvider : ADataProvider<Vector3>
	{
		protected override Vector3[] DataArray { get; } = {Vector3.one};
		protected override int[] DaraSizeArray { get; } = {12};
	}
}