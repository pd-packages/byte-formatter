using System;
using System.Collections;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class ArrayDataProvider : AIListDataProvider
	{
		public override bool CanProvide(Type type)
			=> type.IsArray;

		protected override Type GetElementType(Type type) => type.GetElementType();

		protected override IList CreateInstance(Type type, int lenght)
			=> Array.CreateInstance(type, lenght);
	}
}