﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime.Utils;
using Package.ByteFormatter.Generator;
using UnityEngine;

namespace Package.ByteFormatter.ByteValidator.Impls.Providers
{
	public sealed class ByteConvertableDataProvider : IDataProvider
	{
		public bool CanProvide(Type type)
			=> type.ImplementsInterface<IByteConvertable>() && !type.IsAbstract;

		public int GetValueVariants(Type type)
		{
			var properties = type.GetSerializedProperties();
			if (properties.Length == 0)
				return 1;

			return properties
				.Select(propertyInfo => DataProviderService.GetValueVariants(propertyInfo.PropertyType))
				.Max();
		}

		public int GetValueSize(int variantIndex, Type type)
		{
			var properties = type.GetSerializedProperties();
			if (properties.Length == 0)
				return ObjectMap.PROPERTIES_COUNT_SIZE;

			var dataSize = 0;

			foreach (var propertyInfo in properties)
			{
				var propertyType = propertyInfo.PropertyType;
				var index = Mathf.Min(variantIndex, DataProviderService.GetValueVariants(propertyType) - 1);
				dataSize += DataProviderService.GetValueSize(index, propertyType);
			}

			return ObjectMap.PROPERTIES_COUNT_SIZE + properties.Length * ObjectMap.PROPERTY_INFO_SIZE + dataSize;
		}

		public object CreateValue(int variantIndex, Type type)
		{
			var obj = Activator.CreateInstance(type);

			foreach (var propertyInfo in type.GetSerializedProperties())
			{
				var propertyType = propertyInfo.PropertyType;
				var index = Mathf.Min(variantIndex, DataProviderService.GetValueVariants(propertyType) - 1);
				var value = DataProviderService.CreateValue(index, propertyType);
				propertyInfo.SetValue(obj, value);
			}

			return obj;
		}

		public void SetValue(int variantIndex, object obj, PropertyInfo propertyInfo)
		{
			var value = CreateValue(variantIndex, propertyInfo.PropertyType);
			propertyInfo.SetValue(obj, value);
		}

		public List<string> AssertValueEquals(int variantIndex, object obj, Type type)
		{
			var asserts = new List<string>();
			foreach (var propertyInfo in type.GetSerializedProperties())
			{
				var propertyType = propertyInfo.PropertyType;
				var value = propertyInfo.GetValue(obj);
				var index = Mathf.Min(variantIndex, DataProviderService.GetValueVariants(propertyType) - 1);
				var assertValueEquals = DataProviderService.AssertValueEquals(index, value, propertyType);
				if (assertValueEquals.Count > 0)
					asserts.Add($"{type.Name} property {propertyType.Name}: {assertValueEquals.MergeString()}");
			}

			return asserts;
		}
	}
}