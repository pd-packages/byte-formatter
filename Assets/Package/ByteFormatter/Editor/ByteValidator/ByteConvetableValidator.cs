﻿using System;
using System.Collections.Generic;
using System.Linq;
using ByteFormatter.Runtime;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime.Utils;
using NUnit.Framework;
using Package.ByteFormatter.ByteValidator.Impls;
using Package.ByteFormatter.Generator.Utils;
using UnityEngine;

namespace Package.ByteFormatter.ByteValidator
{
	public class ByteConvetableValidator
	{
		public static void Validate()
		{
			var types = AssemblyHelper.GetTypes();
			var byteFormatterTypeMapper =
				types.SingleOrDefault(f => f.FullName == "ByteFormatter.Runtime.ByteFormatterTypeMapper");
			if (byteFormatterTypeMapper == null)
			{
				Debug.LogError(
					$"[{nameof(ByteConvetableValidator)}] No ByteFormatterTypeMapper in project. Please generate serialization first.");
				return;
			}

			byteFormatterTypeMapper.GetMethod("Map").Invoke(null, Array.Empty<object>());

			var byteConvertableTypes = types
				.Where(t => t.ImplementsInterface<IByteConvertable>())
				.ToArray();

			var errors = new List<string>();
			foreach (var convertableType in byteConvertableTypes)
			{
				errors.AddRange(ValidateType(convertableType));
			}

			foreach (var error in errors)
			{
				Debug.Log($"[{nameof(ByteConvetableValidator)}] <color=red>{error}</color>");
			}

			if (errors.Count == 0)
				Debug.Log($"[{nameof(ByteConvetableValidator)}] States serialization validation success");
			else
				Assert.That(false);
		}

		private static List<string> ValidateType(Type convertableType)
		{
			var errors = new List<string>();
			var variants = DataProviderService.GetValueVariants(convertableType);

			for (var i = 0; i < variants; i++)
			{
				var expectedSize = DataProviderService.GetValueSize(i, convertableType);
				var writer = new ByteWriter();
				var convertable = (IByteConvertable)DataProviderService.CreateValue(i, convertableType);
				convertable.ToByte(writer);

				var bytes = writer.ToArray();

				if (expectedSize != bytes.Length)
				{
					errors.Add(
						$"Data size not valid fot type {convertableType.Name}. Expected size {expectedSize} current {bytes.Length}");
				}

				try
				{
					var reader = new ByteReader(bytes);
					convertable = (IByteConvertable)Activator.CreateInstance(convertableType);
					convertable.FromByte(reader);
					errors.AddRange(DataProviderService.AssertValueEquals(i, convertable, convertableType));
				}
				catch (Exception e)
				{
					errors.Add($"Cannot read from bytes '{convertableType.Name}': {e}");
				}
			}

			if (errors.Count > 0)
				errors.Insert(0, $"------ Test {convertableType.Name} FAIL -------");

			return errors;
		}
	}
}