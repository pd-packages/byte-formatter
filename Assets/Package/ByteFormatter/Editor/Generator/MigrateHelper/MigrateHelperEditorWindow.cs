﻿using System.IO;
using Package.ByteFormatter.ByteValidator;
using UnityEditor;
using UnityEngine;

namespace Package.ByteFormatter.Generator.MigrateHelper
{
	public class MigrateHelperEditorWindow : EditorWindow
	{
		private const string GENERATION_PATH_KEY = "MigrateHelper.Generation.Path";

		private static readonly string DefaultGenerationPath = Path.Combine("Generated", "MigrateModels");

		private string _generationPath;

		[MenuItem("Tools/ByteFormatter/Migrator/Settings")]
		public static void OpenSettingsWindowButton()
		{
			var window = GetWindowWithRect<MigrateHelperEditorWindow>(new Rect(0, 0, 300, 50), false, "Migrator");
			window.Show();
		}

		[MenuItem("Tools/ByteFormatter/Migrator/Generate")]
		public static void GenerateButton() => Generate();

		private void OnEnable()
		{
			_generationPath = GetGenerationPath();
		}

		private void OnGUI()
		{
			EditorGUI.BeginChangeCheck();
			_generationPath = GUILayout.TextField(_generationPath);
			if (EditorGUI.EndChangeCheck())
			{
				SaveGenerationPath();
			}

			if (GUILayout.Button("Generate"))
				Generate();
		}

		private static string GetGenerationPath() => EditorPrefs.GetString(GENERATION_PATH_KEY, DefaultGenerationPath);

		private void SaveGenerationPath()
		{
			EditorPrefs.SetString(GENERATION_PATH_KEY, _generationPath);
		}

		private static void Generate()
		{
			ByteConvetableValidator.Validate();
			var generationPath = Path.Combine(Application.dataPath, GetGenerationPath());
			var directoryInfo = new DirectoryInfo(generationPath);
			MigrateHelperGenerator.Generate(directoryInfo);
		}
	}
}