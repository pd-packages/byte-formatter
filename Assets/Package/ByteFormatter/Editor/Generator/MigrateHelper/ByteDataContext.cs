﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime.Utils;

namespace Package.ByteFormatter.Generator.MigrateHelper
{
	public class ByteDataContext : IClassSyntax
	{
		public readonly Type Type;
		public readonly ByteDataContextAttribute Attribute;

		public string CompilationUnitSyntax { get; set; }
		public string ClassName { get; set; }

		public ByteDataContext(Type type, ByteDataContextAttribute attribute)
		{
			Type = type;
			Attribute = attribute;
		}

		public PropertyInfo[] GetProperties() => Type.GetProperties();

		public string[] GetNamespaces()
			=> Type.GetProperties()
				.Select(f =>
				{
					var propertyType = f.PropertyType;
					var namespaces = new[] { propertyType.Namespace };
					var genericArguments = propertyType.GetGenericArguments();
					return genericArguments.Length == 0
						? namespaces
						: namespaces.Union(genericArguments.Select(s => s.Namespace));
				})
				.Aggregate(Enumerable.Empty<string>(), (enumerable, enumerable1) => enumerable.Concat(enumerable1))
				.Where(f => !string.IsNullOrEmpty(f))
				.Distinct()
				.ToArray();

		public string[] GetByteConvertableDependencies()
			=> Type.GetProperties()
				.Select(f => f.PropertyType)
				.Select(GetDependencyName)
				.Where(f => !string.IsNullOrEmpty(f))
				.Distinct()
				.ToArray();

		private static string GetDependencyName(Type type)
		{
			if (type.ImplementsInterface<IList>() && type.GetGenericArguments().Length == 1 &&
			    type.GetGenericArguments()[0].HasAttribute<ByteDataContextAttribute>())
				return type.GetGenericArguments()[0].FullName;

			if (type.IsClass && type.HasAttribute<ByteDataContextAttribute>())
				return type.FullName;

			return string.Empty;
		}
	}
}