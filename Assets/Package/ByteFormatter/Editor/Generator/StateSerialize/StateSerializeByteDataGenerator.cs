﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime.Utils;
using Package.ByteFormatter.Generator.MigrateHelper;
using UnityEngine;

namespace Package.ByteFormatter.Generator.StateSerialize
{
	public class StateSerializeByteDataGenerator : IByteDataGenerator
	{
		public string Create(ByteDataContext[] contexts, ByteDataContext context)
		{
			context.ClassName = context.Type.Name;
			var writeMethods = GetWriteMethods(context);
			var readMethods = GetReadMethods(context);

			var usingDirectives = context.GetNamespaces()
				.Union(new[]
				{
					typeof(IByteConvertable).Namespace,
					typeof(ByteReader).Namespace,
					typeof(ByteWriter).Namespace,
				})
				.Concat(writeMethods.Select(s => s.Namespace))
				.Concat(readMethods.Select(s => s.Namespace))
				.Distinct()
				.Where(s => !string.IsNullOrEmpty(s))
				.Select(StateSerializeGeneratorTemplates.Using)
				.MergeString();

			var writeBlock = GenerateWriteMethodStatement(writeMethods);
			var readBlock = GenerateReadMethodStatement(readMethods);

			return StateSerializeGeneratorTemplates.Class(
				usingDirectives,
				context.Type.Namespace,
				context.ClassName,
				writeBlock,
				readBlock
			);
		}

		private static string GenerateWriteMethodStatement(GeneratedMethod[] methods)
		{
			var lines = new List<string> { StateSerializeGeneratorTemplates.WriteObjectBegin(methods.Length) };
			lines.AddRange(methods.Select(s => StateSerializeGeneratorTemplates.WriteFieldIndex(s.PropertyKey)));
			lines.AddRange(methods.Select(s => StateSerializeGeneratorTemplates.WriteMethod(
				s.PositionIndex,
				s.MethodName,
				s.ArgumentName
			)));
			return lines.MergeString();
		}

		private static GeneratedMethod[] GetWriteMethods(ByteDataContext context)
		{
			var propertyGroups = context.GetProperties()
				.Where(f => f.HasAttribute<PropertyKeyAttribute>())
				.GroupBy(f => f.GetCustomAttribute<PropertyKeyAttribute>().Key)
				.OrderBy(grouping => grouping.Key);

			var writeMethodsHasExceptions = false;
			foreach (var group in propertyGroups)
			{
				if (group.Count() == 1)
					continue;

				writeMethodsHasExceptions = true;
				Debug.LogError(
					$"[{nameof(StateSerializeByteDataGenerator)}] Property key {group.Key} duplicate in {context.ClassName}");
			}

			if (writeMethodsHasExceptions)
				throw new Exception(
					$"[{nameof(StateSerializeByteDataGenerator)}] {context.ClassName} has incorrect structure");

			return propertyGroups.Select((grouping, i) => GetWriteMethod(grouping.Key, i, grouping.First()))
				.ToArray();
		}

		private static GeneratedMethod GetWriteMethod(int propertyKey, int positionIndex, PropertyInfo propertyInfo)
		{
			var generatedMethod = new GeneratedMethod
			{
				PropertyKey = propertyKey,
				PositionIndex = positionIndex,
				ArgumentName = propertyInfo.Name
			};
			var propertyType = propertyInfo.PropertyType;
			var extensionMethod = ByteExtensionCollector.GetWriterMethod(propertyType.GetSerializedType());
			if (extensionMethod == null)
				throw new Exception(
					$"[{nameof(StateSerializeByteDataGenerator)}] No write method for type {propertyType}");

			generatedMethod.Namespace = extensionMethod.Namespace;
			generatedMethod.MethodName = extensionMethod.MethodInfo.Name;
			return generatedMethod;
		}

		private static string GenerateReadMethodStatement(GeneratedMethod[] methods)
		{
			var lines = new List<string> { StateSerializeGeneratorTemplates.ReadObjectBegin() };
			lines.AddRange(methods.Select(
				s => StateSerializeGeneratorTemplates.ReadMethod(s.PropertyKey, s.ArgumentName, s.MethodName))
			);
			lines.Add(StateSerializeGeneratorTemplates.ReadObjectEnd());
			return lines.MergeString();
		}

		private static GeneratedMethod[] GetReadMethods(ByteDataContext context)
		{
			var propertyGroups = context.GetProperties()
				.Where(f => f.HasAttribute<PropertyKeyAttribute>())
				.GroupBy(f => f.GetCustomAttribute<PropertyKeyAttribute>().Key)
				.OrderBy(grouping => grouping.Key);

			var writeMethodsHasExceptions = false;
			foreach (var group in propertyGroups)
			{
				if (group.Count() == 1)
					continue;

				writeMethodsHasExceptions = true;
				Debug.LogError(
					$"[{nameof(StateSerializeByteDataGenerator)}] Property key {group.Key} duplicate in {context.ClassName}");
			}

			if (writeMethodsHasExceptions)
				throw new Exception(
					$"[{nameof(StateSerializeByteDataGenerator)}] {context.ClassName} has incorrect structure");


			return propertyGroups.Select((grouping, i) => GetReadMethod(grouping.Key, i, grouping.First()))
				.ToArray();
		}

		private static GeneratedMethod GetReadMethod(int propertyKey, int positionIndex, PropertyInfo propertyInfo)
		{
			var generatedMethod = new GeneratedMethod
			{
				PropertyKey = propertyKey,
				PositionIndex = positionIndex,
				ArgumentName = propertyInfo.Name
			};
			var propertyType = propertyInfo.PropertyType;
			var extensionMethod = ByteExtensionCollector.GetReaderMethod(propertyType.GetSerializedType());
			if (extensionMethod == null)
				throw new Exception(
					$"[{nameof(StateSerializeByteDataGenerator)}] No read method for type {propertyType}");

			generatedMethod.Namespace = extensionMethod.Namespace;
			generatedMethod.MethodName = extensionMethod.GetMethod(propertyType);
			return generatedMethod;
		}

		private class GeneratedMethod
		{
			public int PropertyKey;
			public int PositionIndex;
			public string Namespace;
			public string MethodName;
			public string ArgumentName;
		}
	}
}