﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using ByteFormatter.Runtime.StateSerialize;
using ByteFormatter.Runtime.Utils;
using Package.ByteFormatter.Generator.MigrateHelper;
using Package.ByteFormatter.Generator.StateSerialize;
using Package.ByteFormatter.Generator.Utils;
using UnityEditor;
using UnityEngine;

namespace Package.ByteFormatter.Generator
{
	public class ByteDataGenerationProvider
	{
		private readonly IByteDataGenerator _generator;

		public ByteDataGenerationProvider(IByteDataGenerator generator)
		{
			_generator = generator;
		}

		public void Generate(DirectoryInfo directoryInfo)
		{
			var byteDataContexts = AssemblyHelper.GetTypes()
				.Where(f => f.HasAttribute<ByteDataContextAttribute>())
				.Select(t => new ByteDataContext(t, t.GetCustomAttribute<ByteDataContextAttribute>()))
				.ToArray();

			GenerateContexts(byteDataContexts);
			var bakingAOT = GenerateBakingAOT(byteDataContexts);

			if (!directoryInfo.Exists)
				directoryInfo.Create();

			WriteToFile(directoryInfo, byteDataContexts);
			WriteToFile(directoryInfo, bakingAOT);

			AssetDatabase.Refresh();
		}

		private void GenerateContexts(ByteDataContext[] contexts)
		{
			var failed = new HashSet<string>();
			var generated = new HashSet<string>();
			var sortedContexts = contexts
				.OrderBy(c => c.GetByteConvertableDependencies().Length)
				.ToList();

			var index = 0;
			while (sortedContexts.Count > 0)
			{
				if (failed.Count == sortedContexts.Count)
					break;

				var context = sortedContexts[index];
				var dependencies = context.GetByteConvertableDependencies();
				if (!generated.IsSupersetOf(dependencies))
				{
					failed.Add(context.Type.FullName);
					index++;
					continue;
				}

				GenerateContext(contexts, context);
				sortedContexts.RemoveAt(index);
				generated.Add(context.Type.FullName);
				failed.Clear();
				index = 0;
			}

			foreach (var f in failed)
			{
				Debug.LogError(
					$"[{nameof(ByteDataGenerationProvider)}] Cannot generate state serialize for type: {f}");
			}
		}

		private void GenerateContext(ByteDataContext[] contexts, ByteDataContext context)
		{
			var dependencies = context.GetByteConvertableDependencies();

			foreach (var dependency in dependencies)
			{
				var dependencyContext = contexts.FirstOrDefault(f => f.Type.FullName == dependency);
				if (dependencyContext == null)
					throw new Exception(
						$"[{nameof(MigrateHelperGenerator)}] Cannot find dependency context {dependency}");
				GenerateContext(contexts, dependencyContext);
			}

			var classDeclarationSyntax = _generator.Create(contexts, context);
			context.CompilationUnitSyntax = classDeclarationSyntax;
		}

		private static ClassSyntax GenerateBakingAOT(ByteDataContext[] byteDataContexts)
		{
			const string className = "ByteFormatterTypeMapper";
			var types = ByteExtensionCollector.WriterExtensionMethodInfos
				.Select(s => s.SerializedType)
				.Where(s => !s.IsGenericType && !s.IsArray && !s.IsInterface && !s.IsAbstract)
				.ToList();

			types.AddRange(types
				.Where(s => s.IsValueType && Nullable.GetUnderlyingType(s) == null)
				.Select(s => typeof(Nullable<>).MakeGenericType(s))
			);

			types.AddRange(byteDataContexts.Select(s => s.Type));

			types = types.Distinct().ToList();

			var valid = true;

			foreach (var type in types)
			{
				var serializedType = type.GetSerializedType();
				var writeValid = ByteExtensionCollector.ValidateWrite(serializedType);
				if (!writeValid)
					Debug.LogError(
						$"[{nameof(ByteDataGenerationProvider)}] No write method for type '{serializedType.FullName}'");

				var readValid = ByteExtensionCollector.ValidateRead(serializedType);
				if (!readValid)
					Debug.LogError(
						$"[{nameof(ByteDataGenerationProvider)}] No read method for type '{serializedType.FullName}'");

				valid &= writeValid & readValid;
			}

			if (!valid)
			{
				Debug.LogError($"[{nameof(ByteDataGenerationProvider)}] Cannot create baking AOT");
				return new ClassSyntax(
					className,
					StateSerializeGeneratorTemplates.TypeMapper(
						string.Empty,
						"ByteFormatter.Runtime",
						className,
						string.Empty
					)
				);
			}

			var usings = types.Select(s => s.Namespace)
				.Concat(types.Select(s => ByteExtensionCollector.GetWriterMethod(s.GetSerializedType()).Namespace))
				.Concat(types.Select(s => ByteExtensionCollector.GetReaderMethod(s.GetSerializedType()).Namespace))
				.Select(StateSerializeGeneratorTemplates.Using)
				.Distinct()
				.MergeString();

			var bakingMethods = types
				.Select(s => StateSerializeGeneratorTemplates.TypeBakingMethod(
					s.GetName(),
					ByteExtensionCollector.GetWriterMethod(s.GetSerializedType()).GetMethod(s),
					ByteExtensionCollector.GetReaderMethod(s.GetSerializedType()).GetMethod(s)
				))
				.ToList();

			return new ClassSyntax(
				className,
				StateSerializeGeneratorTemplates.TypeMapper(
					usings,
					"ByteFormatter.Runtime",
					className,
					bakingMethods.MergeString()
				)
			);
		}

		private static void WriteToFile(DirectoryInfo directoryInfo, IEnumerable<IClassSyntax> contexts)
		{
			foreach (var context in contexts)
				WriteToFile(directoryInfo, context);
		}

		private static void WriteToFile(DirectoryInfo directoryInfo, IClassSyntax classSyntax)
		{
			var path = Path.Combine(directoryInfo.FullName, classSyntax.ClassName + ".cs");
			File.WriteAllText(path, classSyntax.CompilationUnitSyntax);
		}
	}
}