﻿using Package.ByteFormatter.Generator.MigrateHelper;

namespace Package.ByteFormatter.Generator
{
	public interface IByteDataGenerator
	{
		string Create(ByteDataContext[] contexts, ByteDataContext context);
	}
}