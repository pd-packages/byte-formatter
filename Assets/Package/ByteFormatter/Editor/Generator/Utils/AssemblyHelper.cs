﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Package.ByteFormatter.Generator.Utils
{
	public static class AssemblyHelper
	{
		public static IEnumerable<Type> GetTypes()
			=> AppDomain.CurrentDomain
				.GetAssemblies()
				.Select(f => f.GetTypes())
				.Aggregate((types, types1) => types.Concat(types1).ToArray());
	}
}