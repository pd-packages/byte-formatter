using ByteFormatter.Runtime.StateSerialize;
using UnityEngine;

namespace ByteFormatter.Runtime
{
	[ByteExtension]
	public static class ByteWriterUnityExtensions
	{
		public static void Write(this ByteWriter writer, Vector2 value)
		{
			writer.Write(value.x);
			writer.Write(value.y);
		}

		public static void Write(this ByteWriter writer, Vector2Int value)
		{
			writer.Write(value.x);
			writer.Write(value.y);
		}

		public static void Write(this ByteWriter writer, Vector3 value)
		{
			writer.Write(value.x);
			writer.Write(value.y);
			writer.Write(value.z);
		}

		public static void Write(this ByteWriter writer, Vector4 value)
		{
			writer.Write(value.x);
			writer.Write(value.y);
			writer.Write(value.z);
			writer.Write(value.w);
		}

		public static void Write(this ByteWriter writer, Color value)
		{
			writer.Write(value.r);
			writer.Write(value.g);
			writer.Write(value.b);
			writer.Write(value.a);
		}

		public static void Write(this ByteWriter writer, Color32 value)
		{
			writer.Write(value.r);
			writer.Write(value.g);
			writer.Write(value.b);
			writer.Write(value.a);
		}

		public static void Write(this ByteWriter writer, Quaternion value)
		{
			writer.Write(value.x);
			writer.Write(value.y);
			writer.Write(value.z);
			writer.Write(value.w);
		}

		public static void Write(this ByteWriter writer, Rect value)
		{
			writer.Write(value.xMin);
			writer.Write(value.yMin);
			writer.Write(value.width);
			writer.Write(value.height);
		}

		public static void Write(this ByteWriter writer, RectInt value)
		{
			writer.Write(value.xMin);
			writer.Write(value.yMin);
			writer.Write(value.width);
			writer.Write(value.height);
		}

		public static void Write(this ByteWriter writer, Plane value)
		{
			writer.Write(value.normal);
			writer.Write(value.distance);
		}

		public static void Write(this ByteWriter writer, Bounds value)
		{
			writer.Write(value.center);
			writer.Write(value.size);
		}

		public static void Write(this ByteWriter writer, Ray value)
		{
			writer.Write(value.direction);
			writer.Write(value.origin);
		}

		public static void Write(this ByteWriter writer, Matrix4x4 value)
		{
			writer.Write(value.m00);
			writer.Write(value.m01);
			writer.Write(value.m02);
			writer.Write(value.m03);
			writer.Write(value.m10);
			writer.Write(value.m11);
			writer.Write(value.m12);
			writer.Write(value.m13);
			writer.Write(value.m20);
			writer.Write(value.m21);
			writer.Write(value.m22);
			writer.Write(value.m23);
			writer.Write(value.m30);
			writer.Write(value.m31);
			writer.Write(value.m32);
			writer.Write(value.m33);
		}
	}
}