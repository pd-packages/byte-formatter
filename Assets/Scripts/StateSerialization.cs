using System;
using System.Collections.Generic;
using System.Linq;
using ByteFormatter.Runtime;
using States;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DefaultNamespace
{
	public class StateSerialization : MonoBehaviour
	{
		private SomeState _someState;

		private void Start()
		{
			ByteFormatterTypeCache.Init();

			_someState = CreateData();

			var writer = new ByteWriter();
			_someState.ToByte(writer);

			var reader = new ByteReader(writer.ToArray());
			var someState = reader.ReadByteConvertable<SomeState>();

			Debug.Log($"[{nameof(StateSerialization)}] States are equal: {_someState.Equals(someState)}");
		}

		private static IEnumerable<string> SplitString(string input, int length)
		{
			for (var i = 0; i < input.Length; i += length)
			{
				if (i + length <= input.Length)
					yield return input.Substring(i, length);
				else
					yield return input[i..];
			}
		}

		private static SomeState CreateData() => new()
		{
			Value = Random.Range(int.MinValue, int.MaxValue),
			User = new UserState
			{
				Value = new UserAddressState
				{
					Value = Random.Range(int.MinValue, int.MaxValue)
				}
			},
			Addresses = Enumerable.Repeat(
				new UserAddressState
				{
					Value = Random.Range(int.MinValue, int.MaxValue)
				},
				5
			).ToList(),
			ListOfStrings = Enumerable.Repeat("SomeTextHere", 5).ToList(),
			StringByGuid = Enumerable.Range(0, 5)
				.Select(_ => (Guid.NewGuid(), "SomeTextHere"))
				.ToDictionary(tuple => tuple.Item1, tuple => tuple.Item2),
			UserAddressStateByGuid = Enumerable.Range(0, 5)
				.Select(_ => (Guid.NewGuid(), new UserAddressState
				{
					Value = Random.Range(int.MinValue, int.MaxValue)
				}))
				.ToDictionary(tuple => tuple.Item1, tuple => tuple.Item2),
			NullableVector3 = Random.insideUnitSphere,
			ListOfNullableVector3 = Enumerable.Range(0, 10)
				.Select(_ => Random.insideUnitSphere as Vector3?).ToList(),
			ArrayOfNullableVector3 = Enumerable.Range(0, 10)
				.Select(_ => Random.insideUnitSphere as Vector3?).ToArray()
		};
	}
}