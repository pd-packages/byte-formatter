using ByteFormatter.Runtime.StateSerialize;
using UnityEngine.Scripting;

namespace States
{
	[ByteDataContext(nameof(UserAddressState))]
	public partial class UserAddressState
	{
		[PropertyKey(1)] public int Value { get; set; }

		[Preserve]
		public UserAddressState()
		{
		}

		protected bool Equals(UserAddressState other) => Value == other.Value;

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((UserAddressState)obj);
		}

		public override int GetHashCode() => Value;
	}
}