﻿using System;
using System.Collections.Generic;
using System.Linq;
using ByteFormatter.Runtime.StateSerialize;
using UnityEngine;
using UnityEngine.Scripting;

namespace States
{
	[ByteDataContext(nameof(SomeState))]
	public partial class SomeState
	{
		[PropertyKey(1)] public int Value { get; set; }
		[PropertyKey(2)] public UserState User { get; set; }
		[PropertyKey(3)] public List<UserAddressState> Addresses { get; set; }
		[PropertyKey(4)] public Dictionary<Guid, string> StringByGuid { get; set; }
		[PropertyKey(6)] public Dictionary<Guid, UserAddressState> UserAddressStateByGuid { get; set; }
		[PropertyKey(8)] public List<string> ListOfStrings { get; set; }
		[PropertyKey(9)] public Vector3? NullableVector3 { get; set; }
		[PropertyKey(10)] public List<Vector3?> ListOfNullableVector3 { get; set; }
		[PropertyKey(11)] public Vector3?[] ArrayOfNullableVector3 { get; set; }

		public NotSerializedData Data { get; set; }

		[Preserve]
		public SomeState()
		{
		}

		protected bool Equals(SomeState other)
			=> Value == other.Value
			   && Equals(User, other.User)
			   && Addresses.SequenceEqual(other.Addresses)
			   && StringByGuid.SequenceEqual(other.StringByGuid)
			   && UserAddressStateByGuid.SequenceEqual(other.UserAddressStateByGuid)
			   && ListOfStrings.SequenceEqual(other.ListOfStrings)
			   && NullableVector3.Equals(other.NullableVector3)
			   && ListOfNullableVector3.SequenceEqual(other.ListOfNullableVector3)
			   && ArrayOfNullableVector3.SequenceEqual(other.ArrayOfNullableVector3);

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((SomeState)obj);
		}

		public override int GetHashCode()
			=> HashCode.Combine(Value, User, Addresses, StringByGuid, UserAddressStateByGuid, ListOfStrings);
	}

	public class NotSerializedData
	{
	}
}