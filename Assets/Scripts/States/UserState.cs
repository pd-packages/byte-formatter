using ByteFormatter.Runtime.StateSerialize;
using UnityEngine.Scripting;

namespace States
{
	[ByteDataContext(nameof(UserState))]
	public partial class UserState
	{
		[PropertyKey(1)] public UserAddressState Value { get; set; }

		[Preserve]
		public UserState()
		{
		}

		protected bool Equals(UserState other) => Equals(Value, other.Value);

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((UserState)obj);
		}

		public override int GetHashCode() => (Value != null ? Value.GetHashCode() : 0);
	}
}