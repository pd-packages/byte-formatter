using ByteFormatter.Runtime.MigrateHelper.Impls;
using ByteFormatter.Runtime.MigrateHelper;
using ByteFormatter.Runtime;

namespace MigrateHelper
{
    //------------------------------------------------------------------------------
    // <auto-generated>
    //     This code was generated by MigrateHelperGenerator.
    //
    //     Changes to this file may cause incorrect behavior and will be lost if
    //     the code is regenerated.
    // </auto-generated>
    //------------------------------------------------------------------------------

	public static partial class UserAddressStateByteData
	{
		public static readonly V_0_4_ByteData V_0_4 = new V_0_4_ByteData();

    	[DataVersion("UserAddressState", "0.4")]
    	public class V_0_4_ByteData : IByteData
    	{
			public readonly Int32ByteData Value = Int32ByteData.Instance;
	
    	    public void Transfer(ByteReader reader, ByteWriter writer)
    	    {
				Value.Transfer(reader, writer);
    	    }
	
    	    public void Skip(ByteReader reader)
    	    {
				Value.Skip(reader);
    	    }
    	}
	}
}